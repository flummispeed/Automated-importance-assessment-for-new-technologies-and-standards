# Automated importance assessment for new technologies and standards

Prototype of a master thesis (http://ros.swe.informatik.uni-goettingen.de/#/edit)

Author: Florian Unger

Georg August Universität Göttingen

Institute of Computer Science

Software Engineering for Distributed Systems Group (http://www.swe.informatik.uni-goettingen.de/)

Supervisor: 

Prof. Dr. phil.-nat. Jens Grabowski (http://www.swe.informatik.uni-goettingen.de/staff/jens-grabowski)

Dr. rer. nat. Steffen Herbold (http://www.swe.informatik.uni-goettingen.de/staff/steffen-herbold)

## Getting Started
The client uses Angular 2 (https://angular.io/docs/ts/latest/)

The server side uses PHP (http://php.net/)

### Prerequisites
Have a look in package.json



-- Truncate tables
SET foreign_key_checks = 0;
TRUNCATE category;
TRUNCATE category_item;
TRUNCATE evaluation;
TRUNCATE model;
TRUNCATE model_category;
TRUNCATE model_factor;
TRUNCATE model_field;
TRUNCATE model_field_category_list;

SET foreign_key_checks = 1;

-- Set auto_increment
SET @@auto_increment_increment=1;
SET @@auto_increment_offset=1;

-- Insert Categories
INSERT INTO category (name) VALUES ('SmallMediumLarge'), ('LowMediumHigh'), ('percentage10steps'), ('history') ;
INSERT INTO category_item (name, category_id)
  SELECT 'no value', category.id FROM category WHERE category.name = 'SmallMediumLarge';
INSERT INTO category_item (name, category_id)
  SELECT 'Small', category.id FROM category WHERE category.name = 'SmallMediumLarge';
INSERT INTO category_item (name, category_id)
  SELECT 'Medium', category.id FROM category WHERE category.name = 'SmallMediumLarge';
INSERT INTO category_item (name, category_id)
  SELECT 'Large', category.id FROM category WHERE category.name = 'SmallMediumLarge';
INSERT INTO category_item (name, category_id)
  SELECT 'no value', category.id FROM category WHERE category.name = 'LowMediumHigh';
INSERT INTO category_item (name, category_id)
  SELECT 'Low', category.id FROM category WHERE category.name = 'LowMediumHigh';
INSERT INTO category_item (name, category_id)
  SELECT 'Medium', category.id FROM category WHERE category.name = 'LowMediumHigh';
INSERT INTO category_item (name, category_id)
  SELECT 'High', category.id FROM category WHERE category.name = 'LowMediumHigh';
INSERT INTO category_item (name, category_id)
  SELECT 'no value', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT '0 %', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT '10 %', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT '20 %', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT '30 %', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT '40 %', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT '50 %', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT '60 %', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT '70 %', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT '80 %', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT '90 %', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT '100 %', category.id FROM category WHERE category.name = 'percentage10steps';
INSERT INTO category_item (name, category_id)
  SELECT 'no value', category.id FROM category WHERE category.name = 'history';
INSERT INTO category_item (name, category_id)
  SELECT 'descending', category.id FROM category WHERE category.name = 'history';
INSERT INTO category_item (name, category_id)
  SELECT 'unchanged', category.id FROM category WHERE category.name = 'history';
INSERT INTO category_item (name, category_id)
  SELECT 'rising', category.id FROM category WHERE category.name = 'history';


-- Insert Evaluation
INSERT INTO evaluation (typ) VALUES ('automated'),('category'), ('hand');

-- Insert Model
INSERT INTO model (name) VALUES ('Relevance');

INSERT INTO model_category (name, model_id)
  SELECT 'Interest', model.id FROM model WHERE model.name = 'Relevance';
INSERT INTO model_category (name, model_id)
  SELECT 'Evolution', model.id FROM model WHERE model.name = 'Relevance';
INSERT INTO model_category (name, model_id)
  SELECT 'Education', model.id FROM model WHERE model.name = 'Relevance';
INSERT INTO model_category (name, model_id)
  SELECT 'Power / Potency', model.id FROM model WHERE model.name = 'Relevance';

-- -- Interest
INSERT INTO model_factor (name, model_category_id)
  SELECT 'Organizations', model_category.id FROM model_category WHERE model_category.name = 'Interest';
INSERT INTO model_factor (name, model_category_id)
  SELECT 'Topic on Conference', model_category.id FROM model_category WHERE model_category.name = 'Interest';
INSERT INTO model_factor (name, model_category_id)
  SELECT 'Conferences', model_category.id FROM model_category WHERE model_category.name = 'Interest';
INSERT INTO model_factor (name, model_category_id)
  SELECT 'Forums', model_category.id FROM model_category WHERE model_category.name = 'Interest';
INSERT INTO model_factor (name, model_category_id)
  SELECT 'Search Hits', model_category.id FROM model_category WHERE model_category.name = 'Interest';
INSERT INTO model_factor (name, model_category_id)
  SELECT 'Downloads', model_category.id FROM model_category WHERE model_category.name = 'Interest';
INSERT INTO model_factor (name, model_category_id)
  SELECT 'Views', model_category.id FROM model_category WHERE model_category.name = 'Interest';
INSERT INTO model_factor (name, model_category_id)
  SELECT 'Referencing Documents', model_category.id FROM model_category WHERE model_category.name = 'Interest';
INSERT INTO model_factor (name, model_category_id)
  SELECT 'Paper', model_category.id FROM model_category WHERE model_category.name = 'Interest';
INSERT INTO model_factor (name, model_category_id)
  SELECT 'Books', model_category.id FROM model_category WHERE model_category.name = 'Interest';
INSERT INTO model_factor (name, model_category_id)
  SELECT 'People Involved', model_category.id FROM model_category WHERE model_category.name = 'Interest';

-- -- -- Fields

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'number of involved in the development', model_factor.id, evaluation.id
    FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND evaluation.typ = 'hand'
      AND model_factor.name = 'Organizations';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'on how many conferences', model_factor.id, evaluation.id
    FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Topic on Conference' AND evaluation.typ = 'hand';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'how often it was the major topic', model_factor.id, evaluation.id
    FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Conferences' AND evaluation.typ = 'hand';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'how many forums exists', model_factor.id, evaluation.id
    FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Forums' AND evaluation.typ = 'hand';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'how many forums users exists', model_factor.id, evaluation.id
    FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Forums' AND evaluation.typ = 'hand';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'how many subjects does the forums have', model_factor.id, evaluation.id
    FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Forums' AND evaluation.typ = 'hand';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'how many messages does the forums contains', model_factor.id, evaluation.id
    FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Forums' AND evaluation.typ = 'hand';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'Google', model_factor.id, evaluation.id
    FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Search Hits' AND evaluation.typ = 'automated';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'DuckDuckGo', model_factor.id, evaluation.id
    FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Search Hits' AND evaluation.typ = 'automated';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'downloads of the standard', model_factor.id, evaluation.id
    FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Downloads' AND evaluation.typ = 'hand';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'how many download sources', model_factor.id, evaluation.id
    FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Downloads' AND evaluation.typ = 'hand';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'page views of the standard page', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Views' AND evaluation.typ = 'automated';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'how many documents reference this standard', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Referencing Documents' AND evaluation.typ = 'hand';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'how many papers exists on the standard website', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Paper' AND evaluation.typ = 'hand';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'Google Scholar Hits', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Paper' AND evaluation.typ = 'automated';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'Springer Link Kits', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Paper' AND evaluation.typ = 'automated';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'Google Books Hits', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Books' AND evaluation.typ = 'automated';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'Amazon Hits', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'Books' AND evaluation.typ = 'automated';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'How many People are involved in the development', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Interest'
      AND model_factor.name = 'People Involved' AND evaluation.typ = 'hand';

-- --Evolution
INSERT INTO model_factor (name, model_category_id)
  SELECT 'Download History', model_category.id FROM model_category WHERE model_category.name = 'Evolution';

INSERT INTO model_factor (name, model_category_id)
  SELECT 'Search History', model_category.id FROM model_category WHERE model_category.name = 'Evolution';

INSERT INTO model_factor (name, model_category_id)
  SELECT 'View History', model_category.id FROM model_category WHERE model_category.name = 'Evolution';

INSERT INTO model_factor (name, model_category_id)
  SELECT 'People Involved', model_category.id FROM model_category WHERE model_category.name = 'Evolution';


-- -- -- Fields

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'complete history', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'Download History' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'Download History'
	    AND model_field.name =  'complete history';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'last year', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
  WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'Download History' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'Download History'
      AND model_field.name =  'last year';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'last month', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
  WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'Download History' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'Download History'
      AND model_field.name =  'last month';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'complete history', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
  WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'Search History' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'Search History'
	    AND model_field.name =  'complete history';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'last year', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
  WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'Search History' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'Search History'
      AND model_field.name =  'last year';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'last month', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
  WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'Search History' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'Search History'
      AND model_field.name =  'last month';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'complete history', model_factor.id, evaluation.id
  FROM  model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'View History' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'View History'
	    AND model_field.name =  'complete history';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'last year', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
  WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'View History' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'View History'
      AND model_field.name =  'last year';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'last month', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
  WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'View History' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'View History'
      AND model_field.name =  'last month';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'complete history', model_factor.id, evaluation.id
  FROM  model_category, model_factor, evaluation
    WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'People Involved' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM  model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'People Involved'
	    AND model_field.name =  'complete history';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'last year', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
  WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'People Involved' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'People Involved'
      AND model_field.name =  'last year';

INSERT INTO model_field (name, 	model_factor_id, evaluation_id)
  SELECT 'last month', model_factor.id, evaluation.id
  FROM model_category, model_factor, evaluation
  WHERE model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
      AND model_factor.name = 'People Involved' AND evaluation.typ = 'category';

INSERT INTO model_field_category_list( modell_field_id, category_id )
  SELECT model_field.id, category.id
    FROM model_category, model_factor, model_field, category
    WHERE category.name =  'history'
	    AND model_factor.id = model_field.model_factor_id
	    AND model_category.id = model_factor.model_category_id
      AND model_category.name = 'Evolution'
	    AND model_factor.name = 'People Involved'
      AND model_field.name =  'last month';
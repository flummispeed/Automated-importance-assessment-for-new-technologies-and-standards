<?php

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Class Manual
 *
 * Describes a manual which supports the user
 */
class Manual
{
    /**
     * Manual that describes something.
     * @var string
     */
    public $manual = '';

    /**
     * URL's that provide help.
     * @var array
     */
    public $url;

    /**
     * Manual constructor.
     *
     * @param $xml
     */
    public function __construct($xml)
    {
        $this->manual = $xml->manual . '';
        $this->url = array();
        foreach ($xml->url as $xmlUrl)
        {
            $url = $xmlUrl->uri . '';
            $querySign = '?';
            foreach ($xmlUrl->querySign as $sign) {
                $querySign = $sign;
            }
            $url .= $querySign . '';
            $i = 1;
            foreach ($xmlUrl->parameterList->parameter as $param) {
                if ($i > 1) {
                    $url .= '&';
                }
                $url .= $param . '';
                $i++;
            }
            array_push($this->url, $url);
        }
    }
}
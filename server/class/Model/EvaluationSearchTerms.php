<?php

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Class EvaluationSearchTerms
 * Describes search terms to support the user.
 */
class EvaluationSearchTerms
{
    /**
     * Array of terms.
     * @var array
     */
    public $searchTerms = array();

    /**
     * Is an automatic fill to be done?
     * @var bool
     */
    public $automaticFill = false;

    /**
     * Describes the task of the user.
     * @var string
     */
    public $taskDescription = '';

    /**
     * How many results schould be shown?
     * @var int
     */
    public $numberOfResults = 1;

    /**
     * EvaluationSearchTerms constructor.
     * Ctreate an object based on the XML-file describing it.
     * @param $xml
     */
    function __construct($xml)
    {
        foreach ($xml->searchTermList->searchTerm as $key => $term)
        {
            array_push($this->searchTerms, $term . '');
        }
        $this->automaticFill = stringToBool($xml->automaticFill . '');
        $this->taskDescription = $xml->taskDescription . '';
        $this->numberOfResults = $xml->numberOfResults * 1;
    }
}
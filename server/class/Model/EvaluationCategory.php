<?php

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Class EvaluationCategory
 *
 * Describes of evaluations.
 */
class EvaluationCategory
{
    /**
     * Notation of the category to be used.
     * @var string
     */
    public $category = '';

    /**
     * @var EvaluationSearchTerms[]
     */
    public $searchTerms = array();

    /**
     * @var EvaluationView[]
     */
    public $webViews = array();

    /**
     * Manual to explain the evaluation.
     * @var EvaluationHelp
     */
    public $help;

    /**
     * EvaluationCategory constructor.
     *
     * @param $xml
     */
    function __construct($xml)
    {
        //extract the category name
        foreach($xml[0]->attributes() as $key => $value)
        {
            if($key == 'categoryName')
            {
                $this->category = $value . '';
            }
        }

        //extract manual
        foreach($xml as $key => $value)
        {
            if($key == 'help')
            {
                $this->help = new Manual($value);
            }
        }
        foreach ($xml->webViewList->webView as $item)
        {
            $webView = new EvaluationView($item);
            array_push($this->webViews, $webView);
        }
        foreach ($xml->searchTermsList->searchTerms as $item)
        {
            $searchTerm = new EvaluationSearchTerms($item);
            array_push($this->searchTerms, $searchTerm);
        }
    }
}
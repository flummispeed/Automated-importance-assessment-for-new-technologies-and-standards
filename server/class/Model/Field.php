<?php

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Class Field
 * A field describes everything regarding the measuring.
 */
class Field
{
    /**
     * Name of the field.
     * @var string
     */
    public $name = '';
    /**
     * Array of evaluations regading the field.
     * @var Evaluation[]
     */
    public $evaluations;

    /**
     * Manual to support the user.
     * @var Manual
     */
    public $description;

    /**
     * Field constructor.
     *
     * @param $xml
     */
    public function __construct($xml)
    {
        $this->name = $xml->name . '';
        $this->evaluations = array();
        foreach($xml->evaluationList->evaluation as $evaluation)
        {
            $this->addEvaluation(new Evaluation($evaluation));
        }
        foreach ($xml->description as $description)
        {
            $this->description = new Manual($description);
        }
    }

    /**
     * Adding an evaluation to this field.
     * @param Evaluation $evaluation
     */
    public function addEvaluation(Evaluation $evaluation)
    {
        array_push($this->evaluations, $evaluation);
    }

    /**
     * Creating the HTML view of this field.
     * @param string $viewType
     */
    public function view($viewType = '')
    {
        if($viewType == ViewType::INPUT)
        {
            $i = 0;
            foreach($this->evaluations as $evaluation)
            {
                $evaluation->view($viewType);
                //show all evaluations among each other.
                if($i > 0)
                {
                    echo '</br>';
                }
                $i ++;
            }
        }
    }
}
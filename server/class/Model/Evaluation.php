<?php

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
class Evaluation
{
    /**
     * Unique identifier of each single evaluation.
     * The id is used to identify the values in the database.
     * @var int
     */
    public $id = 0;

    /**
     * evaluation type
     * @var string EvaluationType
     */
    public $type = '';

    /**
     * @var EvaluationManual
     */
    public $manual;

    /**
     * @var EvaluationCategory
     */
    public $category;

    /**
     * @var EvaluationMethod
     */
    public $method;

    /**
     * Evaluation constructor.
     *
     * @param $xml
     */
    public function __construct($xml)
    {
        //extract the id
        foreach($xml[0]->attributes() as $key => $value)
        {
            if($key == 'id')
            {
                $this->id = $value * 1;
            }
        }
        //get out type
        foreach($xml as $key => $value)
        {
            if($key == EvaluationType::CATEGORY)
            {
                $this->type = EvaluationType::CATEGORY;
                $this->category = new EvaluationCategory($value);
            }
            else if($key == EvaluationType::METHOD)
            {
                $this->type = EvaluationType::METHOD;
                $this->method = new EvaluationMethod($value);
            }
            else if($key == EvaluationType::MANUAL_VALUE)
            {
                $this->type = EvaluationType::MANUAL_VALUE;
                $this->manual = new EvaluationManual($value);
            }
        }
    }

    /**
     * HTML view.
     * @param string $viewType
     */
    public function view($viewType = '')
    {
        if($viewType == ViewType::INPUT)
        {
            if($this->type == EvaluationType::MANUAL_VALUE)
            {
                echo '<input '.$this->manual->getType().' name="'.$this->id.'">';
            }
            if($this->type == EvaluationType::METHOD)
            {
                $this->callMethods($this->method->method);
            }
        }
    }

    /**
     * Call defined methods.
     * @param $call
     */
    private function callMethods($call)
    {
        global $searchTerm;
        if($call == EvaluationMethods::GOOGLE_SEARCH_HITS_NUMBER)
        {
            $method = new Google();
            echo $method->searchHits($searchTerm);
        }
    }

    private function storeValue($value, $type)
    {
        //not implemented yet
    }
}

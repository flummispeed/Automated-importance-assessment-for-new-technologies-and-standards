<?php
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
/**
 * Class Model
 *
 */
class Model
{
    /**
     * @var ROSLogger
     */
    //private  $LOGGER;
    /**
     * The name of the model.
     * @var string
     */
    public $name = '';

    /**
     * The unique identifier of the model.
     * @var int
     */
    public $id = 0;
    /**
     * The categories that could be used by modelFields.
     * @var Category[]
     */
    public $categories;

    /**
     * The categories of the model.
     * @var ModelCategory[]
     */
    public $modelCategories;
    /**
     * Model is constructed by an XML schema.
     * @param string $filename
     */
    public function __construct($filename)
    {
        //$this->LOGGER = new ROSLogger();

        if (file_exists($filename))
        {
            $xml = simplexml_load_file($filename);

            $this->name = $xml->name . '';
            $this->id = $xml->id * 1;
            $this->categories = array();

            global $categoryList;

            //add all field categories to the model
            foreach($xml->categoryList->category as $category)
            {
                $key = $category->name . "";
                $i = 0;
                foreach($category->itemList->item as $item)
                {
                    $value = $item . '';
                    $categoryList[$key][$i] = $value;
                    $i ++;
                }
                array_push($this->categories, new Category($category));
            }

            $this->modelCategories = array();
            //add all model categories to the model
            foreach($xml->modelCategoryList->modelCategory as $modelCat)
            {
                array_push($this->modelCategories, new ModelCategory($modelCat));
            }
        }
        else
        {
            //TODO Log Problem
        }
    }

    /**
     * Show collected Data
     * @param string $viewType
     */
    public function view($viewType = ''){
        //show data
        echo '<div id="dataBox">';
        foreach($this->modelCategories as $category)
        {
            echo '<div id="partHeadline"><strong id="'.hash('adler32',$category->name . '').'">'.$category->name.'</strong></div>'. "\n";
            echo '<div id="partData">'. "\n";
            $category->view(1, $viewType);
            echo '</div>'. "\n";
        }
        echo '</div>';
    }

    /**
     * Show a list of hyperlinks of the ModelCategories in the first layer.
     */
    public function viewModelCategoriesAsHyperlinkList()
    {
        foreach($this->modelCategories as $category)
        {
            echo '<li><a href="#'.hash('adler32',$category->name . '').'"> '.$category->name.'</a></li>';
        }
    }

}
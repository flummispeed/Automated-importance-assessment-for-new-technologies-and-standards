<?php

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Class EvaluationManual
 *
 * Manual to support the user
 */
class EvaluationManual
{
    /**
     * Data type to be stored.
     * @var string
     */
    public $type = '';

    /**
     * MySQL int min
     * @var int
     */
    public $min = -2147483648;

    /**
     * MySQL int max
     * @var int
     */
    public $max = 2147483647;

    /**
     * Step size
     * @var int
     */
    public $stepSize = 1;

    /**
     * Default value or in case of a hyperlink list the label
     * @var string
     */
    public $default = '';

    /**
     * @var EvaluationSearchTerms[]
     */
    public $searchTerms = array();

    /**
     * @var EvaluationView[]
     */
    public $webViews = array();

    /**
     * Manual to explain the evaluation.
     * @var EvaluationHelp
     */
    public $help;

    /**
     * EvaluationManual constructor.
     * Creates object based on the correlated XML file.
     * @param $xml
     */
    function __construct($xml)
    {
        foreach($xml as $key => $value)
        {
            if($key == 'help')
            {
                $this->help = new Manual($value);
            }
            if($key == EvaluationManualInputDataTypes::INT)
            {
                $this->type = EvaluationManualInputDataTypes::INT;
            }
            if($key == EvaluationManualInputDataTypes::DOUBLE)
            {
                $this->type = EvaluationManualInputDataTypes::DOUBLE;
            }
            if($key == EvaluationManualInputDataTypes::HYPERLINK_LIST)
            {
                $this->type = EvaluationManualInputDataTypes::HYPERLINK_LIST;
                $this->default = $value->label . '';
            }
            if($key == EvaluationManualInputDataTypes::TEXT)
            {
                $this->type = EvaluationManualInputDataTypes::TEXT;
            }

            if($key == EvaluationManualInputDataTypes::INT || $key == EvaluationManualInputDataTypes::DOUBLE)
            {
                foreach($value as $aKey => $aValue)
                {
                    if($aKey == 'min')
                    {
                        $this->min = $aValue * 1;
                    }
                    if($aKey == 'max')
                    {
                        $this->max = $aValue * 1;
                    }
                    if($aKey == 'stepsize')
                    {
                        $this->stepSize = $aValue * 1;
                    }
                    if($aKey == 'default')
                    {
                        $this->default = $aValue . '';
                    }
                }
            }
        }
        foreach ($xml->webViewList->webView as $item)
        {
            $webView = new EvaluationView($item);
            array_push($this->webViews, $webView);
        }
        foreach ($xml->searchTermsList->searchTerms as $item)
        {
            $searchTerm = new EvaluationSearchTerms($item);
            array_push($this->searchTerms, $searchTerm);
        }
    }

    /**
     * Creates a type in JSON.
     * @return string
     */
    public function getType(){
        if($this->type == EvaluationManualInputDataTypes::DOUBLE || $this->type == EvaluationManualInputDataTypes::INT)
        {
            return 'type="number" min="'.$this->min.'" max="'.$this->max.'" step="'.$this->stepSize.'" value="'.$this->default.'"';
        }
        else{
            return 'type="text"';
        }

    }
}
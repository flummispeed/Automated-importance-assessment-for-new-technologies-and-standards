<?php

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Class EvaluationView
 * Describes a view to support the user in measuring a value.
 */
class EvaluationView
{
    /**
     * Description of the task to the user.
     * @var string
     */
    public $description = '';

    /**
     * URL to be used.
     * @var string
     */
    public $url = '';

    /**
     * Type of quotes to be used in the request.
     * @var string
     */
    public $queryQuotes = '';

    /**
     * Parameters to be added.
     * @var string
     */
    public $urlParam = '';

    /**
     * EvaluationView constructor.
     * Creates an object based on the XML file describing it.
     * @param $xml
     */
    function __construct($xml)
    {
        foreach ($xml as $key => $value) {
            if ($key == 'description') {
                $this->description = $value . '';
            }
            if ($key == 'url') {

                $querySign = '?';
                foreach ($value->querySign as $sign) {
                    $querySign = $sign;
                }
                $this->url = $value->uri . $querySign;

                $urlParams = '';
                $i = 1;
                foreach ($value->parameterList->parameter as $param) {
                    if ($i > 1) {
                        $urlParams .= '&';
                    }
                    $urlParams .= $param;
                    $i++;
                }
                foreach ($value->queryParam as $qParam) {
                    if ($i > 1) {
                        $urlParams .= '&';
                    }
                    $urlParams .= $qParam . '=';
                }
                foreach ($value->queryParamQuotes as $qQuotes)
                {
                    $this->queryQuotes = $qQuotes . '';
                }
                $this->urlParam = $urlParams;
            }
        }
    }
}
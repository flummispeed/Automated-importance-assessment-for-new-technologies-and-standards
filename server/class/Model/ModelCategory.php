<?php

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Class ModelCategory
 * Categories which belongs to the Model. In an FCM this are the factors and criteria.
 */
class ModelCategory
{
    public $name = '';
    /**
     * @var ModelCategory[]
     */
    public $modelCategories;
    /**
     * @var Field[]
     */
    public $modelFields;

    public $help;

    /**
     * ModelCategory constructor.
     * XML structure will be parsed regarding the categories.
     *
     * @param $xml
     */
    public function __construct($xml)
    {
        $this->name = $xml->name . '';
        $this->modelFields = array();
        foreach($xml->fieldList->field as $field)
        {
            $this->addField($field);
        }

        $this->modelCategories = array();
        foreach($xml->modelCategoryList->modelCategory as $cat)
        {
            $this->addSubCategory($cat);
        }
        if(property_exists($xml, 'help'))
        {
            $this->help = new Manual($xml->help);
        }
    }

    /**
     * Add a subordinated category.
     * @param $xml
     */
    public function addSubCategory($xml)
    {
        array_push($this->modelCategories, new ModelCategory($xml));
    }

    /**
     * Add a field to this category.
     * @param $xml
     */
    public function addField($xml)
    {
        array_push($this->modelFields, new Field($xml));
    }

    /**
     * Create the view of this category in HTML form.
     * @param int    $depth
     * @param string $viewType
     */
    public function view($depth = 1, $viewType = '')
    {
        $shifting = '';
        for($i = 1; $i<$depth; $i++)
        {
            $shifting .= '&nbsp;&nbsp;&nbsp;&nbsp;';
        }
        if($depth > 1)
        {
            echo $shifting .'<strong>' . $this->name . '</strong><br>'. "\n";
        }

        if(count($this->modelFields) > 0 )
        {
            echo '<div id="valuePartGroup"><table border="0">'. "\n";
            $colorChanger = false;
            foreach($this->modelFields as $field)
            {
                if($colorChanger)
                {
                    echo '<tr style="background-color: rgb(255, 251, 167)">';
                }
                else
                {
                    echo '<tr>';
                }
                //show field name
                echo '<td width="350px" align="left">' . $field->name . ':</td>'. "\n";

                //show field data
                echo '<td align="left" valign="bottom">';
                if($viewType == ViewType::INPUT)
                {
                    $field->view($viewType);
                }
                else
                {
                    echo 'no value';
                }
                echo '</td>';
                echo '</tr> '. "\n";

                //flip to get clear difference between lines of the table
                $colorChanger = !$colorChanger;
            }
            echo '</table></div>'. "\n";
        }
        $depth++;
        foreach($this->modelCategories as $category)
        {
            $category->view($depth, $viewType);
        }

    }
}
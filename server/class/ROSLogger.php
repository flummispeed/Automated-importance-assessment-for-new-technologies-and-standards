<?php
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Class ROSLogger
 * Logging class if necessary to watch the server side
 */
class ROSLogger{

    private $log = array();
    private $logSevere = array();
    private $logWarning = array();
    private $logInfo = array();

    public function __construct(){

    }

    public function log(ROSLog $log){
        if($log->getLevel() == Level::SEVERE){
            array_push($this->logSevere, $log);
            return;
        }
        elseif($log->getLevel() == Level::WARNING){
            array_push($this->logWarning, $log);
            return;
        }
        elseif($log->getLevel() == Level::INFO){
            array_push($this->logInfo, $log);
            return;
        }else{
            array_push($this->log, $log);
        }
    }

    public function getLog($level = ''){
        $log = '';

        if($level == Level::SEVERE){
            $log .= $this->getSevereLog();
        }
        elseif($level == Level::WARNING){
            $log .= $this->getWarningLog();
        }
        elseif($level == Level::WARNING){
            $log .= $this->getInfoLog();
        }else{
            $log = $this->getSevereLog();
            $log .= $this->getWarningLog();
            $log .= $this->getInfoLog();
            $log .= $this->getNonLevelLog();

        }
        return $log;
    }

    private function getSevereLog(){
        $return = '';
        foreach($this->logSevere as $log){
            $return .= "Level: <b>" . $log->getLevel() . " </b>";
            $return .= $log-> getMessage() . " </br>";
        }
        return $return;
    }

    private function getWarningLog(){
        $return = '';
        foreach($this->logWarning as $log){
            $return .= "Level: <b>" . $log->getLevel() . " </b>";
            $return .= $log-> getMessage() . " </br>";
        }
        return $return;
    }

    private function getInfoLog(){
        $return = '';
        foreach($this->logInfo as $log){
            $return .= "Level: <b>" . $log->getLevel() . " </b>";
            $return .= $log-> getMessage() . " </br>";
        }
        return $return;
    }

    private function getNonLevelLog(){
        $return = '';
        foreach($this->log as $log){
            $return .= "Level: <b>" . $log->getLevel() . " </b>";
            $return .= $log-> getMessage() . " </br>";
        }
        return $return;
    }

    public function isSevere(){
        return empty($this->logSevere);
    }
    public function isWarning(){
        return empty($this->logWarning);
    }
    public function isInfo(){
        return empty($this->logInfo);
    }



}

class ROSLog{
    private $Level;
    private $message;

    public function __construct($level = '', $message = ''){
        $this->Level = $level;
        $this->message = $message;
        $this->logToFile();
    }

    public function getLevel(){
        return $this->Level;
    }
    public function getMessage(){
        return $this->message;
    }

    private function logToFile(){
        $file = fopen('../log/log.txt', 'w');
        if($file){
            fwrite($file, $this->Level . " " . $this->message . " \n");
        }
    }
}

class Level{
    const SEVERE = 'SEVERE';
    const WARNING = 'WARNING';
    const INFO = 'INFO';

    public function __construct(){

    }
}
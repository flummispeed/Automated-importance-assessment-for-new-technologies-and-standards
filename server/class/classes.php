<?php
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Collection of classes
 */
require 'Category/Category.php';
require 'Category/Item.php';

require 'Evaluation/Google.php';

require 'Manual/Manual.php';

require 'Model/Model.php';
require 'Model/Field.php';
require 'Model/Evaluation.php';
require 'Model/EvaluationCategory.php';
require 'Model/EvaluationMethod.php';
require 'Model/EvaluationSearchTerms.php';
require 'Model/EvaluationView.php';
require 'Model/EvaluationManual.php';
require 'Model/EvaluationHelp.php';
require 'Model/ModelCategory.php';

require 'Search/SearchDone.php';
require 'Search/SearchTerm.php';
require 'Search/SearchTopic.php';
require 'Search/SearchValue.php';


require 'ROSLogger.php';

require 'ROSmysqli.php';

require 'utilityClasses.php';
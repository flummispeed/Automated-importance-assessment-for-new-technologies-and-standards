<?php

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Class Google
 *
 * Measuring a search request.
 */
class Google
{

    /**
     * Returns the number of search hits.
     * @param string $value
     *
     * @return string
     */
    public function searchHits($value = '')
    {
        $this->datatype = EvaluationInputDataTypes::INT;
        //search team
        $searchLink = 'https://www.google.de/search?num=1&q=' . $value;
        //echo $searchLink;
        $data = file_get_contents($searchLink);
        //where too look for the number of search hits
        $resultTag = 'id="resultStats"';
        $temp = explode( ' ', substr($data, strpos($data, $resultTag)+ strlen($resultTag),100));

        return $this->cleanNumber($temp[1]);
    }

    /**
     * Parse string number to integer.
     * @param $number
     *
     * @return string
     */
    private function cleanNumber($number)
    {
        $temp = explode('.', $number);
        $result = '';
        foreach($temp as $value)
        {
            $result .= $value;
        }
        return $result;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 09.05.2016
 * Time: 17:38
 *//**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
/**
 * Class ViewType
 * All types that are possible by calling a view of a model.
 */
class ViewType{
    const INPUT = 'INPUT';
    const SHOW = 'SHOW';
}

/**
 * Class EvaluationTypes
 * Possible evaluation types.
 */
class EvaluationType{
    const METHOD = 'method';
    const MANUAL_VALUE = 'manualValue';
    const CATEGORY = 'category';
}

/**
 * Class EvaluationMethods
 * All methods that could be used for evaluation.
 */
class EvaluationMethods{
    const GOOGLE_SEARCH_HITS_NUMBER = 'GoogleSearchHitsNumber';
    const DUCK_DUCK_GO_SEARCH_HITS_NUMBER = 'DuckDuckGoSearchHitsNumber';
}

/**
 * Class EvaluationManualInputDataTypes
 * Represents the possible data types
 */
class EvaluationManualInputDataTypes{
    const INT = 'int';
    const DOUBLE = 'double';
    const TEXT = 'text';
    const HYPERLINK_LIST = 'hyperlinkList';
}

/**
 * Class EvaluationInputDataTypes
 * Data types including varchars.
 */
class EvaluationInputDataTypes extends EvaluationManualInputDataTypes{
    const VARCHAR = 'varchar';
}

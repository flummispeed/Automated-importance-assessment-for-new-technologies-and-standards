<?php

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Class Category
 * 
 * Category for measuring valuies.
 */
class Category
{
    public $name = '';
    /**
     * @var Item[]
     */
    public $items;

    /**
     * Category constructor.
     * @param $xml
     *      XML category object.
     */
    public function __construct($xml)
    {
        $this->name = $xml->name . '';
        $this->items = array();
        foreach($xml->itemList->item as $item)
        {
            $this->addItem(new Item($item . ''));
        }
    }

    /**
     * Add an Item to the category.
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        array_push($this->items, $item);
    }


}
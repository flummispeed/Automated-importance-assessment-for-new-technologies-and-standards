<?php

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Class SearchDone
 * This class basically represents the structure of the database table search_done.
 * A search done represents the execution of a search to a search term. For example the 'Ajax Tools Framework (ATF)'
 * would be a search term of the search topic 'Eclipse'. The timestamp '13.06.2016 13:04' of a search done than
 * could represent the execution of the search of the search term 'Ajax Tools Framework (ATF)'.
 */
class SearchDone
{
    /*
     * $id, $timestamp and $searchTermId will be used to generate JSON objects. Therefore these attributes are public.
     */
    public $id = 0;
    public $timestamp = '';
    public $searchTermId = 0;

    /**
     * SearchDone constructor.
     * @param $id
     * @param $timestamp
     * @param $searchTermId
     */
    function __construct($id, $timestamp, $searchTermId)
    {
        $this->id = $id;
        $this->timestamp = $timestamp;
        $this->searchTermId = $searchTermId;
    } 
}
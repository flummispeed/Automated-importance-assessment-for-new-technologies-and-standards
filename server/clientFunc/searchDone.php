<?php
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
require '../class/classes.php';
require '../config/config.php';
require '../func/func.php';

header('Content-type: application/json');
header('Cache-Control: no-cache, must-revalidate');

/**
 * Database connection.
 */
$mysqli = new ROSmysqli();

$search = json_decode(file_get_contents("php://input"));

$query = "INSERT INTO search_done (searchTerm_id) VALUES ('".$search->searchTermId."' )";

$mysqli ->query($query);

$searchDoneId = $mysqli->insert_id;

$query = "SELECT  `id` , DATE_FORMAT(  `timestamp` ,  '%d.%m.%Y - %H:%i' ) AS searchTime,  `searchTerm_id` FROM  `search_done` WHERE id=".$searchDoneId;

$searchDone = FALSE;
if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
        $searchDone = '{"id":' . $row['id'];
        $searchDone .= ',"timestamp":"' . $row['searchTime'];
        $searchDone .= '","searchTermId":' . $row['searchTerm_id'];
        $searchDone .= '}';
    }
}

echo $searchDone;
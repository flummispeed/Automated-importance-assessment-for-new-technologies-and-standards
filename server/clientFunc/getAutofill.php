<?php
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
require '../class/classes.php';
require '../config/config.php';
require '../func/func.php';

header('Content-type: application/json');
header('Cache-Control: no-cache, must-revalidate');

/*
 * List of autofill values will be generated regarding the request
 */

/**
 * Database connection.
 */
$mysqli = new ROSmysqli();

/**
 * Database table that should be used.
 */
$table = 'search_topic';
/**
 * Columns in the database table that should be selected.
 */
$columns = 'id AS id';
/**
 * Restrictions.
 */
$where = '';

if($_GET['table'] == "term")
{
    $table = 'search_term';
    $columns = $columns . ', term AS value';

    if (isset($_GET['foreignkey']))
    {
        $where = "WHERE searchTopic_id = " . $_GET['foreignkey'];
    }
}

if($_GET['table'] == "search")
{
    $table = 'search_done';
    $columns = $columns . ", DATE_FORMAT(  `timestamp` ,  '%d.%m.%Y - %H:%i' ) AS value";

    if (isset($_GET['foreignkey']))
    {
        $where = "WHERE searchTerm_id = " . $_GET['foreignkey'];
    }
}

if($table == 'search_topic')
{
    $columns = $columns . ', topic AS value';
    $where = "WHERE model = 1";
}

/**
 * MySQL query to load values for auto completion.
 */
$query = 'SELECT ' . $columns .
    ' FROM ' . $table . ' ' . $where;

/**
 * Array of values for auto completion
 * @var SearchValue[]
 */
$autoFill = [];

if($result = $mysqli ->query($query))
{
    while($row = $result->fetch_assoc())
    {
       array_push($autoFill, new SearchValue($row['id'], $row['value']));
    }
    echo json_encode($autoFill);
}
else
{
    echo 'false';
}
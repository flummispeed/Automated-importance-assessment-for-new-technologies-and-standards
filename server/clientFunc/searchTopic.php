<?php
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

require '../class/classes.php';
require '../config/config.php';
require '../func/func.php';

header('Content-type: application/json');
header('Cache-Control: no-cache, must-revalidate');

/**
 * Database connection.
 */
$mysqli = new ROSmysqli();

$topic = json_decode(file_get_contents("php://input"));

$query = "INSERT INTO search_topic (topic, model) VALUES ('".$topic->topic."', '".$topic->model."' )";

$mysqli ->query($query);

//JSON
echo '{"id":'.$mysqli->insert_id.',"topic":"'.$topic->topic.'","model":'.$topic->model.'}';
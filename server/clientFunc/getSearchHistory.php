<?php
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
require '../class/classes.php';
require '../config/config.php';
require '../func/func.php';

header('Content-type: application/json');
header('Cache-Control: no-cache, must-revalidate');

/**
 * Database connection.
 */
$mysqli = new ROSmysqli();

/**
 * Array with all topics that belongs to a given or the default model
 */
$topics = [];

//MySQL WHERE clause
$where = '';

//Will be set to false in case one of the queries fails
$isQueryComplete = true;

//check whether a model is given
if (isset($_GET['model'])) {
    $where = "WHERE model = " . $_GET['model'];
} else {
    $where = "WHERE model = 1";
}

//query all search topics
$query = "SELECT id, topic, model FROM  `search_topic` " . $where . " ORDER BY topic";

if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
        $searchTopic = new SearchTopic($row['id'] * 1, $row['topic'], $row['model'] * 1);
        array_push($topics, $searchTopic);
    }
} else {
    $isQueryComplete = false;
    echo 'FALSE';
}

foreach ($topics as $topic) {
    $query = "SELECT id, term, searchTopic_id FROM `search_term` WHERE searchTopic_id = " . $topic->id . " ORDER BY term ";
    if ($result = $mysqli->query($query)) {
        while ($row = $result->fetch_assoc()) {
            $searchTerm = new SearchTerm($row['id'] * 1, $row['term'], $row['searchTopic_id'] * 1);
            array_push($topic->terms, $searchTerm);
        }
    } else {
        $isQueryComplete = false;
        echo 'FALSE';
    }
    foreach ($topic->terms as $term) {
        $query = "SELECT  id, DATE_FORMAT(  `timestamp` ,  '%d.%m.%Y - %H:%i' ) AS value, searchTerm_id FROM search_done WHERE searchTerm_id=" . $term->id . " ORDER BY `timestamp` ";
        if ($result = $mysqli->query($query)) {
            while ($row = $result->fetch_assoc()) {
                $searchDone = new SearchDone($row['id'] * 1, $row['value'], $row['searchTerm_id'] *1);
                array_push($term->searchDone, $searchDone);
            }
        } else {
            $isQueryComplete = false;
            echo 'FALSE';
        }
    }
}
//JSON
if ($isQueryComplete) {
    echo json_encode($topics);
}
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

export class Category
{
    public static idCat:number = 0;
    id:number;
    name:string;
    items:CategoryItem[] = [];

    constructor()
    {
    }

    deserialize(object)
    {
        this.id = Category.idCat;
        Category.idCat =Category.idCat + 1;
        this.name = object.name;
        for(var item of object.items)
        {
            let categoryItem = new CategoryItem();
            categoryItem.value = item.name;
            categoryItem.id = CategoryItem.idCount;
            CategoryItem.idCount = CategoryItem.idCount + 1;
            this.items.push(categoryItem);
        }
    }

    selectItem(item:CategoryItem)
    {
        for(var catItem of this.items)
        {
            if(item.id == catItem.id)
            {
                catItem.backgroundColor = 'rgba(177, 250, 185, 1)';
            }
            else
            {
                catItem.backgroundColor = 'rgba(255, 255, 255, 1)';
            }
        }
    }

    deselectAllItems()
    {
        for(var catItem of this.items)
        {
            catItem.backgroundColor = 'rgba(255, 255, 255, 1)';
        }
    }

    selectItemByValue(value)
    {
        for(var catItem of this.items)
        {
            if(value == catItem.value)
            {
                catItem.backgroundColor = 'rgba(177, 250, 185, 1)';
            }
            else
            {
                catItem.backgroundColor = 'rgba(255, 255, 255, 1)';
            }
        }
    }

    clone()
    {
        let clone = new Category();
        for(var item of this.items)
        {
            let it = new CategoryItem();
            it.id = CategoryItem.getId();
            it.value =item.value;
            clone.items.push(it);
        }
        clone.name = this.name;
        clone.id = Category.idCat;
        Category.idCat = Category.idCat + 1;
        return clone;
    }
}

export class CategoryItem
{
    public static idCount:number = 0;

    id:number = 0;
    value:string = '';
    backgroundColor:string = 'rgba(255, 255, 255, 1)';

    public static getId()
    {
        CategoryItem.idCount = CategoryItem.idCount +1;
        return CategoryItem.idCount;
    }
}
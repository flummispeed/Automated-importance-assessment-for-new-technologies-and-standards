/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var Category = (function () {
    function Category() {
        this.items = [];
    }
    Category.prototype.deserialize = function (object) {
        this.id = Category.idCat;
        Category.idCat = Category.idCat + 1;
        this.name = object.name;
        for (var _i = 0, _a = object.items; _i < _a.length; _i++) {
            var item = _a[_i];
            var categoryItem = new CategoryItem();
            categoryItem.value = item.name;
            categoryItem.id = CategoryItem.idCount;
            CategoryItem.idCount = CategoryItem.idCount + 1;
            this.items.push(categoryItem);
        }
    };
    Category.prototype.selectItem = function (item) {
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var catItem = _a[_i];
            if (item.id == catItem.id) {
                catItem.backgroundColor = 'rgba(177, 250, 185, 1)';
            }
            else {
                catItem.backgroundColor = 'rgba(255, 255, 255, 1)';
            }
        }
    };
    Category.prototype.deselectAllItems = function () {
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var catItem = _a[_i];
            catItem.backgroundColor = 'rgba(255, 255, 255, 1)';
        }
    };
    Category.prototype.selectItemByValue = function (value) {
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var catItem = _a[_i];
            if (value == catItem.value) {
                catItem.backgroundColor = 'rgba(177, 250, 185, 1)';
            }
            else {
                catItem.backgroundColor = 'rgba(255, 255, 255, 1)';
            }
        }
    };
    Category.prototype.clone = function () {
        var clone = new Category();
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var item = _a[_i];
            var it_1 = new CategoryItem();
            it_1.id = CategoryItem.getId();
            it_1.value = item.value;
            clone.items.push(it_1);
        }
        clone.name = this.name;
        clone.id = Category.idCat;
        Category.idCat = Category.idCat + 1;
        return clone;
    };
    Category.idCat = 0;
    return Category;
}());
exports.Category = Category;
var CategoryItem = (function () {
    function CategoryItem() {
        this.id = 0;
        this.value = '';
        this.backgroundColor = 'rgba(255, 255, 255, 1)';
    }
    CategoryItem.getId = function () {
        CategoryItem.idCount = CategoryItem.idCount + 1;
        return CategoryItem.idCount;
    };
    CategoryItem.idCount = 0;
    return CategoryItem;
}());
exports.CategoryItem = CategoryItem;
//# sourceMappingURL=category.js.map
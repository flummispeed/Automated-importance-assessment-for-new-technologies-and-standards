/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * Indicate the current page's location within a navigational hierarchy.
 * http://getbootstrap.com/components/#breadcrumbs
 */
export class Breadcrumb
{
    /**
     * Counter to get identifiers.
     * @type {number}
     */
    public static idCount:number = 0;

    /**
     * id of the object.
     * @type {number}
     */
    private id:number = 0;

    /**
     * Value to be displayed.
     * @type {string}
     */
    public value = '';
    /**
     * CSS class to be used.
     * @type {string}
     */
    public class = '';

    constructor()
    {
        Breadcrumb.idCount ++;
        this.id = Breadcrumb.idCount;
    }

    getId():number
    {
        return this.id;
    }

}

/**
 * Manage page locations within a navigational hierarchy.
 */
export class Breadcrumbs
{
    elements:Breadcrumb[] = [];

    /**
     * Add new breadcrumb to list of breadcrumbs.
     * @param value
     */
    new( value:string )
    {
        for ( var item of this.elements )
        {
            item.class = '';
        }
        let breadcrumb = new Breadcrumb ();
        breadcrumb.value = value;
        breadcrumb.class = 'active';
        this.elements.push ( breadcrumb );
    }

    /**
     * Remove breadcrumbs that are older than actual value.
     * @param value
     */
    back( id:number )
    {
        let position:number = 0;
        let i:number = 0;
        for ( var breadcrumb of this.elements )
        {
            if ( breadcrumb.getId() == id )
            {
                breadcrumb.class = 'active';
                position = i;
            }
            i = i + 1;
        }


        let length = this.elements.length;
        if ( length > 1 )
        {
            for ( var j = position + 1; j < length; j++ )
            {
                this.elements.pop ();
            }
        }
    }
}
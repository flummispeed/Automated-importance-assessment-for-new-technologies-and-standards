/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
/**
 * Indicate the current page's location within a navigational hierarchy.
 * http://getbootstrap.com/components/#breadcrumbs
 */
var Breadcrumb = (function () {
    function Breadcrumb() {
        /**
         * id of the object.
         * @type {number}
         */
        this.id = 0;
        /**
         * Value to be displayed.
         * @type {string}
         */
        this.value = '';
        /**
         * CSS class to be used.
         * @type {string}
         */
        this.class = '';
        Breadcrumb.idCount++;
        this.id = Breadcrumb.idCount;
    }
    Breadcrumb.prototype.getId = function () {
        return this.id;
    };
    /**
     * Counter to get identifiers.
     * @type {number}
     */
    Breadcrumb.idCount = 0;
    return Breadcrumb;
}());
exports.Breadcrumb = Breadcrumb;
/**
 * Manage page locations within a navigational hierarchy.
 */
var Breadcrumbs = (function () {
    function Breadcrumbs() {
        this.elements = [];
    }
    /**
     * Add new breadcrumb to list of breadcrumbs.
     * @param value
     */
    Breadcrumbs.prototype.new = function (value) {
        for (var _i = 0, _a = this.elements; _i < _a.length; _i++) {
            var item = _a[_i];
            item.class = '';
        }
        var breadcrumb = new Breadcrumb();
        breadcrumb.value = value;
        breadcrumb.class = 'active';
        this.elements.push(breadcrumb);
    };
    /**
     * Remove breadcrumbs that are older than actual value.
     * @param value
     */
    Breadcrumbs.prototype.back = function (id) {
        var position = 0;
        var i = 0;
        for (var _i = 0, _a = this.elements; _i < _a.length; _i++) {
            var breadcrumb = _a[_i];
            if (breadcrumb.getId() == id) {
                breadcrumb.class = 'active';
                position = i;
            }
            i = i + 1;
        }
        var length = this.elements.length;
        if (length > 1) {
            for (var j = position + 1; j < length; j++) {
                this.elements.pop();
            }
        }
    };
    return Breadcrumbs;
}());
exports.Breadcrumbs = Breadcrumbs;
//# sourceMappingURL=breadcrumb.js.map
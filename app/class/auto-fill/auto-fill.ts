/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {SearchValue} from "../search/search-value";

export class AutoFill
{
    public autoFills:SearchValue[] = [];

    public query = '';
    public list = [];
    public filteredList = [];

    public errorMessage:string;



    fillList()
    {
        this.list = []; // clear list to get the actual state of the list.
        for ( var item of this.autoFills )
        {
            this.list.push ( item.value );
        }
    }

    /**
     * Filter the auto fill list.
     */
    filter()
    {
        this.fillList ();
        if ( this.query !== "" )
        {
            this.filteredList = this.list.filter ( function ( el )
            {
                return el.toLowerCase ().indexOf ( this.query.toLowerCase () ) > -1;
            }.bind ( this ) );
        }
        else
        {
            this.filteredList = [];
        }
    }

    /**
     * Return the id of the query term.
     * @returns {number}
     */
    getSearchId()
    {
        for ( var item of this.autoFills )
        {
            if ( item.value == this.query )
            {
                return item.id;
            }
        }
        return 0;
    }

}
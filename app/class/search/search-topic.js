"use strict";
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
/**
 * Class SearchTopic
 * A search topic is the generic term of the search and can have different search terms which belongs to it. For example
 * the 'Ajax Tools Framework (ATF)' would be a search term of the search topic 'Eclipse'.
 */
var SearchTopic = (function () {
    function SearchTopic() {
        this.id = 0;
        this.topic = '';
        this.model = 1;
        this.terms = [];
    }
    return SearchTopic;
}());
exports.SearchTopic = SearchTopic;
//# sourceMappingURL=search-topic.js.map
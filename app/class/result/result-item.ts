/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

/**
 * The result of am evaluation of a field of a model category of a model.
 */
export class ResultItem
{
    /**
     * Value to be stored in database.
     */
    value:any = '';

    /**
     * Stores if value can not be found. Due to this value it is possible to distinguish between
     * value not set and no value can not be found.
     * @type {string}
     */
    valueCanNotBeFound:string = 'val';

    /**
     * Type of the value to be stored.
     */
    type = '';

    /**
     * Id of the evaluation to be stored. This id references the evaluation of a field of a model category of a model.
     * @type {number}
     */
    idEvaluation = -1;

}
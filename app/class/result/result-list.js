/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var ResultList = (function () {
    function ResultList() {
        this.results = [];
    }
    ResultList.prototype.addResult = function (result) {
        if (!this.resultExists(result.idSearch)) {
            this.results.push(result);
        }
    };
    ResultList.prototype.resultExists = function (idSearch) {
        for (var _i = 0, _a = this.results; _i < _a.length; _i++) {
            var res = _a[_i];
            if (res.idSearch == idSearch) {
                return true;
            }
        }
        return false;
    };
    ResultList.prototype.listLength = function () {
        return this.results.length;
    };
    ResultList.prototype.removeResult = function (idSearch) {
        var newResults = [];
        for (var _i = 0, _a = this.results; _i < _a.length; _i++) {
            var result = _a[_i];
            if (idSearch != result.idSearch) {
                newResults.push(result);
            }
        }
        this.results = newResults;
    };
    ResultList.prototype.getKeyOfResults = function (idSearch) {
        for (var i = 0; i < this.results.length; i++) {
            var result = this.results[i];
            if (result.idSearch = idSearch) {
                return i;
            }
        }
        return -1;
    };
    return ResultList;
}());
exports.ResultList = ResultList;
//# sourceMappingURL=result-list.js.map
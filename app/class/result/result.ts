/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {SearchTerm} from "../search/search-term";
import {ResultItem} from "./result-item";

/**
 * Evaluation result to be stored in database
 */
export class Result
{
    /**
     * SearchTerm that is evaluated.
     */
    public term:SearchTerm;
    /**
     * List of results.
     * @type {Array}
     */
    public resultItems:ResultItem[] = [];

    public idSearch:number = 0;

    public searchTime:string;

    getResultOnId( id )
    {
        id = id * 1;
        for ( var item of this.resultItems )
        {
            if ( item.idEvaluation == id )
            {
                if(item.valueCanNotBeFound == 'noValA')
                {
                    return '';
                }
                return item.value;
            }
        }
        return 'noValue';
    }

    getResultItemOnId( id )
    {
        id = id * 1;
        for ( var item of this.resultItems )
        {
            if ( item.idEvaluation == id )
            {
                return item;
            }
        }
        return new ResultItem ();
    }
}
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var result_item_1 = require("./result-item");
/**
 * Evaluation result to be stored in database
 */
var Result = (function () {
    function Result() {
        /**
         * List of results.
         * @type {Array}
         */
        this.resultItems = [];
        this.idSearch = 0;
    }
    Result.prototype.getResultOnId = function (id) {
        id = id * 1;
        for (var _i = 0, _a = this.resultItems; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.idEvaluation == id) {
                if (item.valueCanNotBeFound == 'noValA') {
                    return '';
                }
                return item.value;
            }
        }
        return 'noValue';
    };
    Result.prototype.getResultItemOnId = function (id) {
        id = id * 1;
        for (var _i = 0, _a = this.resultItems; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.idEvaluation == id) {
                return item;
            }
        }
        return new result_item_1.ResultItem();
    };
    return Result;
}());
exports.Result = Result;
//# sourceMappingURL=result.js.map
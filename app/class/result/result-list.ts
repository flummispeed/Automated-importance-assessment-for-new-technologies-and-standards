/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {Result} from "./result";

export class ResultList
{
    public results:Result[] = [];

    public addResult( result:Result )
    {
        if ( !this.resultExists ( result.idSearch ) )
        {
            this.results.push ( result );
        }
    }

    public resultExists( idSearch )
    {
        for ( var res of this.results )
        {
            if ( res.idSearch == idSearch )
            {
                return true;
            }
        }
        return false;
    }

    public  listLength()
    {
        return this.results.length;
    }

    public removeResult( idSearch:number )
    {
        let newResults:Result[] = [];
        for ( var result of this.results )
        {
            if ( idSearch != result.idSearch )
            {
                newResults.push ( result );
            }
        }
        this.results = newResults;
    }

    public getKeyOfResults( idSearch:number )
    {
        for ( var i = 0; i < this.results.length; i++ )
        {
            let result:Result = this.results[ i ];
            if ( result.idSearch = idSearch )
            {
                return i;
            }
        }
        return -1;
    }
}
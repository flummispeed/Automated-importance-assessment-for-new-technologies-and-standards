/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var manual_1 = require('../manual/manual');
var model_evaluation_1 = require("./evaluation/model-evaluation");
var ModelField = (function () {
    function ModelField() {
        this.evaluation = [];
    }
    /**
     * Deserialize JSON object
     * @param object
     */
    ModelField.prototype.deserialize = function (object) {
        this.name = object.name;
        for (var _i = 0, _a = object.evaluations; _i < _a.length; _i++) {
            var evaluation = _a[_i];
            var modelEvaluation = new model_evaluation_1.ModelEvaluation();
            modelEvaluation.deserialize(evaluation);
            this.evaluation.push(modelEvaluation);
        }
        if (object.description) {
            var manual = new manual_1.Manual();
            manual.deserialize(object.description);
            this.manual = manual;
        }
    };
    ModelField.prototype.getEvaluationValues = function () {
        var valueList = [];
        for (var _i = 0, _a = this.evaluation; _i < _a.length; _i++) {
            var evaluation = _a[_i];
            //add a resultItem to the list
            if (evaluation.value.idEvaluation != -1) {
                evaluation.loadResults();
                valueList.push(evaluation.value);
            }
        }
        return valueList;
    };
    ModelField.prototype.isSetEvaluationValues = function () {
        var isSet = true;
        for (var _i = 0, _a = this.evaluation; _i < _a.length; _i++) {
            var evaluationItem = _a[_i];
            if (evaluationItem.value.value == '' && evaluationItem.value.valueCanNotBeFound != 'noValA') {
                isSet = false;
            }
            else if (!evaluationItem.value.value && evaluationItem.value.valueCanNotBeFound != 'noValA') {
                isSet = false;
            }
        }
        return isSet;
    };
    return ModelField;
}());
exports.ModelField = ModelField;
//# sourceMappingURL=model-field.js.map
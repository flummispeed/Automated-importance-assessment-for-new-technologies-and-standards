/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {Manual} from '../manual/manual';
import {ModelEvaluation} from "./evaluation/model-evaluation";

export class ModelField
{
    name:string;
    manual:Manual;
    evaluation:ModelEvaluation[] = [];

    constructor()
    {
    }

    /**
     * Deserialize JSON object
     * @param object
     */
    deserialize( object )
    {
        this.name = object.name;
        for ( var evaluation of object.evaluations )
        {
            let modelEvaluation = new ModelEvaluation ();
            modelEvaluation.deserialize ( evaluation );
            this.evaluation.push ( modelEvaluation );
        }
        if ( object.description )
        {
            let manual = new Manual ();
            manual.deserialize ( object.description );
            this.manual = manual;
        }
    }

    getEvaluationValues()
    {
        let valueList = [];
        for ( var evaluation of this.evaluation )
        {
            //add a resultItem to the list
            if ( evaluation.value.idEvaluation != -1 )
            {
                evaluation.loadResults ();
                valueList.push ( evaluation.value );
            }
        }
        return valueList;
    }

    isSetEvaluationValues():boolean
    {
        let isSet:boolean = true;
        for ( var evaluationItem of this.evaluation )
        {
            if ( evaluationItem.value.value == '' && evaluationItem.value.valueCanNotBeFound != 'noValA' )
            {
                isSet = false;
            }
            else if ( !evaluationItem.value.value && evaluationItem.value.valueCanNotBeFound != 'noValA' )
            {
                isSet = false;

            }

        }
        return isSet;
    }


}
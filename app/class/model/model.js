/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var model_category_1 = require("./model-category");
var model_field_1 = require("./model-field");
var category_1 = require("../category/category");
var categories_1 = require("../category/categories");
var Model = (function () {
    function Model() {
        this.modelCategories = [];
        this.modelFields = [];
    }
    /**
     * Deserialize JSON object
     * @param object
     */
    Model.prototype.deserialize = function (object) {
        this.name = object.name;
        this.id = object.id;
        for (var _i = 0, _a = object.categories; _i < _a.length; _i++) {
            var category = _a[_i];
            var cat = new category_1.Category();
            cat.deserialize(category);
            categories_1.Categories.categories.push(cat);
        }
        for (var _b = 0, _c = object.modelCategories; _b < _c.length; _b++) {
            var modelCat = _c[_b];
            var modelCategory = new model_category_1.ModelCategory();
            modelCategory.deserialize(modelCat);
            this.modelCategories.push(modelCategory);
        }
        if (object.hasOwnProperty('modelFields')) {
            for (var _d = 0, _e = object.modelFields; _d < _e.length; _d++) {
                var field = _e[_d];
                var modelField = new model_field_1.ModelField();
                modelField.deserialize(field);
                this.modelFields.push(modelField);
            }
        }
        console.log(this.toString());
    };
    Model.prototype.getEvaluationValues = function () {
        var resultItems = [];
        //get results of the sub categories
        for (var _i = 0, _a = this.modelCategories; _i < _a.length; _i++) {
            var modelCategory = _a[_i];
            var listResultItems = modelCategory.getEvaluationValues();
            if (listResultItems.length > 0) {
                //copy results of the sub categories to the result list
                for (var _b = 0, listResultItems_1 = listResultItems; _b < listResultItems_1.length; _b++) {
                    var listItem = listResultItems_1[_b];
                    resultItems.push(listItem);
                }
            }
        }
        for (var _c = 0, _d = this.modelFields; _c < _d.length; _c++) {
            var modelField = _d[_c];
            var list = modelField.getEvaluationValues();
            if (list.length > 0) {
                for (var _e = 0, list_1 = list; _e < list_1.length; _e++) {
                    var list2Item = list_1[_e];
                    resultItems.push(list2Item);
                }
            }
        }
        return resultItems;
    };
    return Model;
}());
exports.Model = Model;
//# sourceMappingURL=model.js.map
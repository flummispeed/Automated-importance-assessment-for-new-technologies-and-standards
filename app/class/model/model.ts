/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {ModelCategory} from "./model-category";
import {ModelField} from "./model-field";
import {Category} from "../category/category";
import {Categories} from "../category/categories";

export class Model
{
    id:number;
    name:string;
    modelCategories:ModelCategory[] = [];
    modelFields:ModelField[] = [];

    constructor()
    {
    }

    /**
     * Deserialize JSON object
     * @param object
     */
    deserialize( object )
    {
        this.name = object.name;
        this.id = object.id;

        for ( var category of object.categories )
        {
            let cat = new Category ();
            cat.deserialize ( category );
            Categories.categories.push ( cat );
        }

        for ( var modelCat of object.modelCategories )
        {
            let modelCategory = new ModelCategory ();
            modelCategory.deserialize ( modelCat );
            this.modelCategories.push ( modelCategory );
        }

        if ( object.hasOwnProperty ( 'modelFields' ) )
        {
            for ( var field of object.modelFields )
            {
                let modelField = new ModelField ();
                modelField.deserialize ( field );
                this.modelFields.push ( modelField );
            }
        }

        console.log ( this.toString () );
    }

    getEvaluationValues()
    {
        let resultItems = [];
        //get results of the sub categories
        for ( var modelCategory of this.modelCategories )
        {
            let listResultItems = modelCategory.getEvaluationValues ();
            if ( listResultItems.length > 0 )
            {
                //copy results of the sub categories to the result list
                for ( var listItem of listResultItems )
                {
                    resultItems.push ( listItem );
                }
            }
        }
        for ( var modelField of this.modelFields )
        {
            let list = modelField.getEvaluationValues ();
            if ( list.length > 0 )
            {
                for ( var list2Item of list )
                {
                    resultItems.push ( list2Item );
                }
            }
        }
        return resultItems;
    }

}


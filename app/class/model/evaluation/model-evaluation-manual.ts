import {Manual} from '../../manual/manual';
import {ModelEvaluationSearchTerms} from "./model-evaluation-search-terms";
import {ModelEvaluationView} from "./model-evaluation-view";
import {ModelEvaluationManualHyperlinkList} from "./model-evaluation-manual-hyperlinklist";
/**
 * Created by Florian Unger 2016.
 * Master theses - Automated importance assessment for new technologies and standards
 * http://ros.swe.informatik.uni-goettingen.de/license
 */

export class ModelEvaluationManual
{
    type:string;
    min:number;
    max:number;
    stepsize:number;
    defaultVal:any;
    searchTerms:ModelEvaluationSearchTerms [] = [];
    webViews:ModelEvaluationView [] = [];
    help:Manual;
    hyperlinkList:ModelEvaluationManualHyperlinkList;

    constructor()
    {
    }

    /**
     * Deserialize JSON object
     * @param object
     */
    deserialize( object )
    {
        this.type = object.type;
        if ( this.type == 'hyperlinkList' )
        {
            this.hyperlinkList = new ModelEvaluationManualHyperlinkList();
        }
        this.min = object.min;
        this.max = object.max;
        this.stepsize = object.stepSize;
        this.defaultVal = object.default;
        for ( var searchTermIn of object.searchTerms )
        {
            let searchTerm = new ModelEvaluationSearchTerms ();
            searchTerm.deserialize ( searchTermIn );
            this.searchTerms.push ( searchTerm );
        }
        for ( var webViewIn of object.webViews )
        {
            let webView = new ModelEvaluationView ();
            webView.deserialize ( webViewIn );
            this.webViews.push ( webViewIn );
        }
        if ( object.help != null )
        {
            this.help = new Manual ();
            this.help.deserialize ( object.help );
        }

    }
}
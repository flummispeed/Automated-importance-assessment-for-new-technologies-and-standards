/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var category_1 = require('../../category/category');
var manual_1 = require('../../manual/manual');
var model_evaluation_search_terms_1 = require("./model-evaluation-search-terms");
var model_evaluation_view_1 = require("./model-evaluation-view");
var categories_1 = require("../../category/categories");
var ModelEvaluationCategory = (function () {
    function ModelEvaluationCategory() {
        this.searchTerms = [];
        this.webViews = [];
    }
    /**
     * Deserialize JSON object
     * @param object
     */
    ModelEvaluationCategory.prototype.deserialize = function (object) {
        var category = new category_1.Category();
        category = categories_1.Categories.getCategory(object.category);
        this.category = category.clone();
        for (var _i = 0, _a = object.searchTerms; _i < _a.length; _i++) {
            var searchTermIn = _a[_i];
            var searchTerm = new model_evaluation_search_terms_1.ModelEvaluationSearchTerms();
            searchTerm.deserialize(searchTermIn);
            this.searchTerms.push(searchTerm);
        }
        for (var _b = 0, _c = object.webViews; _b < _c.length; _b++) {
            var webViewIn = _c[_b];
            var webView = new model_evaluation_view_1.ModelEvaluationView();
            webView.deserialize(webViewIn);
            this.webViews.push(webViewIn);
        }
        if (object.help != null) {
            this.help = new manual_1.Manual();
            this.help.deserialize(object.help);
        }
    };
    return ModelEvaluationCategory;
}());
exports.ModelEvaluationCategory = ModelEvaluationCategory;
//# sourceMappingURL=model-evaluation-category.js.map
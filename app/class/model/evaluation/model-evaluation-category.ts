/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {Category} from '../../category/category';
import {Manual} from '../../manual/manual';
import {ModelEvaluationSearchTerms} from "./model-evaluation-search-terms";
import {ModelEvaluationView} from "./model-evaluation-view";
import {Categories} from "../../category/categories";

export class ModelEvaluationCategory
{
    category: Category;
    help: Manual;
    searchTerms:ModelEvaluationSearchTerms [] = [];
    webViews:ModelEvaluationView [] = [];

    constructor()
    {
    }

    /**
     * Deserialize JSON object
     * @param object
     */
    deserialize(object)
    {
        let category = new Category();
        category  = Categories.getCategory(object.category);
        this.category = category.clone();

        for ( var searchTermIn of object.searchTerms )
        {
            let searchTerm = new ModelEvaluationSearchTerms ();
            searchTerm.deserialize ( searchTermIn );
            this.searchTerms.push ( searchTerm );
        }
        for ( var webViewIn of object.webViews)
        {
            let webView = new ModelEvaluationView ();
            webView.deserialize(webViewIn);
            this.webViews.push(webViewIn);
        }
        if(object.help != null)
        {
            this.help = new Manual();
            this.help.deserialize(object.help);
        }
    }
}
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {ModelEvaluationCategory} from "./model-evaluation-category";
import {ModelEvaluationManual} from "./model-evaluation-manual";
import {ResultItem} from "../../result/result-item";

export class ModelEvaluation
{
    id:number;
    category:ModelEvaluationCategory;
    manual:ModelEvaluationManual;
    method:string = '';

    value:ResultItem;

    constructor()
    {
        this.value = new ResultItem;
    }

    /**
     * Deserialize JSON object
     * @param object
     */
    deserialize( object )
    {
        this.id = object.id;
        this.value.idEvaluation = this.id;
        if ( object.type == "manualValue" )
        {
            this.manual = new ModelEvaluationManual;
            this.manual.deserialize ( object.manual );
            this.value.type = this.manual.type;
        }
        else if ( object.type == "method" )
        {
            this.method = object.method;
            this.value.type = 'method';
        }
        else if ( object.type == "category" )
        {
            this.category = new ModelEvaluationCategory ();
            this.category.deserialize ( object.category );
            this.value.type = 'category';
        }
    }

    /**
     * Load all results that are not loaded before e.g. manual.hyperlinkList
     */
    loadResults()
    {
        if ( this.manual )
        {
            if ( this.manual.type == 'hyperlinkList' )
            {
                this.value.value = this.manual.hyperlinkList;
            }
        }
    }
}
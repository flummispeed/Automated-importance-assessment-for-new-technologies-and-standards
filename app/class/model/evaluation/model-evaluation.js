/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var model_evaluation_category_1 = require("./model-evaluation-category");
var model_evaluation_manual_1 = require("./model-evaluation-manual");
var result_item_1 = require("../../result/result-item");
var ModelEvaluation = (function () {
    function ModelEvaluation() {
        this.method = '';
        this.value = new result_item_1.ResultItem;
    }
    /**
     * Deserialize JSON object
     * @param object
     */
    ModelEvaluation.prototype.deserialize = function (object) {
        this.id = object.id;
        this.value.idEvaluation = this.id;
        if (object.type == "manualValue") {
            this.manual = new model_evaluation_manual_1.ModelEvaluationManual;
            this.manual.deserialize(object.manual);
            this.value.type = this.manual.type;
        }
        else if (object.type == "method") {
            this.method = object.method;
            this.value.type = 'method';
        }
        else if (object.type == "category") {
            this.category = new model_evaluation_category_1.ModelEvaluationCategory();
            this.category.deserialize(object.category);
            this.value.type = 'category';
        }
    };
    /**
     * Load all results that are not loaded before e.g. manual.hyperlinkList
     */
    ModelEvaluation.prototype.loadResults = function () {
        if (this.manual) {
            if (this.manual.type == 'hyperlinkList') {
                this.value.value = this.manual.hyperlinkList;
            }
        }
    };
    return ModelEvaluation;
}());
exports.ModelEvaluation = ModelEvaluation;
//# sourceMappingURL=model-evaluation.js.map
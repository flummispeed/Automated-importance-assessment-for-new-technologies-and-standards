/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {ModelField} from "./model-field";
import {Manual} from "../manual/manual";

export class ModelCategory
{
    public static idCount = 0;
    id:number;
    name:String;
    modelCategories:ModelCategory[] = [];
    modelFields:ModelField[] = [];
    help:Manual;

    constructor()
    {
    }

    /**
     * Deserialize JSON object
     * @param object
     */
    deserialize( object )
    {
        this.name = object.name;
        this.id = ModelCategory.idCount;
        ModelCategory.idCount = ModelCategory.idCount + 1;

        for ( var subCat of object.modelCategories )
        {
            let modelCategory = new ModelCategory ();
            modelCategory.deserialize ( subCat );
            this.modelCategories.push ( modelCategory );
        }

        for ( var field of object.modelFields )
        {
            let modelField = new ModelField ();
            modelField.deserialize ( field );
            this.modelFields.push ( modelField );
        }
        this.help = new Manual();
        if(object.help)
        {
            this.help.deserialize(object.help);
        }
    }

    getEvaluationValues()
    {
        let valueList = [];
        for ( var valueM of this.modelCategories )
        {
            let list = valueM.getEvaluationValues ();
            if ( list.length > 0 )
            {
                for ( var listItem of list )
                {
                    valueList.push ( listItem );
                }
            }
        }
        for ( var value of this.modelFields )
        {
            let list2 = value.getEvaluationValues ();
            if ( list2.length > 0 )
            {
                for ( var list2Item of list2 )
                {
                    valueList.push ( list2Item );
                }
            }
        }
        return valueList;
    }

    isSetModelFields():boolean
    {
        let isSet:boolean = true;
        for ( var field of this.modelFields )
        {
            if ( !field.isSetEvaluationValues () )
            {
                isSet = false;
            }
        }
        return isSet;
    }

    isEvaluated()
    {
        let isEvaluated:boolean = true;
        for ( var modelCat of this.modelCategories )
        {
            if ( !modelCat.isEvaluated () )
            {
                isEvaluated = false;
            }
        }
        if ( isEvaluated && this.isSetModelFields () )
        {
            return true;
        }
        return false;
    }

}

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var model_field_1 = require("./model-field");
var manual_1 = require("../manual/manual");
var ModelCategory = (function () {
    function ModelCategory() {
        this.modelCategories = [];
        this.modelFields = [];
    }
    /**
     * Deserialize JSON object
     * @param object
     */
    ModelCategory.prototype.deserialize = function (object) {
        this.name = object.name;
        this.id = ModelCategory.idCount;
        ModelCategory.idCount = ModelCategory.idCount + 1;
        for (var _i = 0, _a = object.modelCategories; _i < _a.length; _i++) {
            var subCat = _a[_i];
            var modelCategory = new ModelCategory();
            modelCategory.deserialize(subCat);
            this.modelCategories.push(modelCategory);
        }
        for (var _b = 0, _c = object.modelFields; _b < _c.length; _b++) {
            var field = _c[_b];
            var modelField = new model_field_1.ModelField();
            modelField.deserialize(field);
            this.modelFields.push(modelField);
        }
        this.help = new manual_1.Manual();
        if (object.help) {
            this.help.deserialize(object.help);
        }
    };
    ModelCategory.prototype.getEvaluationValues = function () {
        var valueList = [];
        for (var _i = 0, _a = this.modelCategories; _i < _a.length; _i++) {
            var valueM = _a[_i];
            var list = valueM.getEvaluationValues();
            if (list.length > 0) {
                for (var _b = 0, list_1 = list; _b < list_1.length; _b++) {
                    var listItem = list_1[_b];
                    valueList.push(listItem);
                }
            }
        }
        for (var _c = 0, _d = this.modelFields; _c < _d.length; _c++) {
            var value = _d[_c];
            var list2 = value.getEvaluationValues();
            if (list2.length > 0) {
                for (var _e = 0, list2_1 = list2; _e < list2_1.length; _e++) {
                    var list2Item = list2_1[_e];
                    valueList.push(list2Item);
                }
            }
        }
        return valueList;
    };
    ModelCategory.prototype.isSetModelFields = function () {
        var isSet = true;
        for (var _i = 0, _a = this.modelFields; _i < _a.length; _i++) {
            var field = _a[_i];
            if (!field.isSetEvaluationValues()) {
                isSet = false;
            }
        }
        return isSet;
    };
    ModelCategory.prototype.isEvaluated = function () {
        var isEvaluated = true;
        for (var _i = 0, _a = this.modelCategories; _i < _a.length; _i++) {
            var modelCat = _a[_i];
            if (!modelCat.isEvaluated()) {
                isEvaluated = false;
            }
        }
        if (isEvaluated && this.isSetModelFields()) {
            return true;
        }
        return false;
    };
    ModelCategory.idCount = 0;
    return ModelCategory;
}());
exports.ModelCategory = ModelCategory;
//# sourceMappingURL=model-category.js.map
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Route} from '@angular/router-deprecated';
import '../../rxjs-operators';

import {DataEvaluationComponent} from '../data/data-evaluation/data-evaluation.component';
import {HomeComponent} from '../home/home.component';
import {HeaderComponent} from '../header/header.component';
import {ModelService} from '../../services/model/model.service';
import {LicenseComponent} from "../license/license.component";
import {DataEditComponent} from "../data/data-edit/data-edit.component";

/**
 * Main component.
 */
@Component ( {
    selector: 'app',
    templateUrl: 'app/components/app/app.component.html',
    directives: [
        ROUTER_DIRECTIVES,
        HeaderComponent
    ],
    providers: [
        ModelService,
    ]
} )

@RouteConfig ( [
    new Route ( {
        path: '/home',
        name: 'Home',
        component: HomeComponent
    } ),
    new Route ( {
        path: '/edit',
        name: 'Edit',
        component: DataEditComponent
    } ),
    new Route ( {
        path: '/evaluation',
        name: 'Evaluation',
        component: DataEvaluationComponent
    } ),
    new Route ( {
        path: '/license',
        name: 'License',
        component: LicenseComponent
    } )

] )

export class AppComponent
{


}
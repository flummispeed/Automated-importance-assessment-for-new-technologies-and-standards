/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
require('../../rxjs-operators');
var data_evaluation_component_1 = require('../data/data-evaluation/data-evaluation.component');
var home_component_1 = require('../home/home.component');
var header_component_1 = require('../header/header.component');
var model_service_1 = require('../../services/model/model.service');
var license_component_1 = require("../license/license.component");
var data_edit_component_1 = require("../data/data-edit/data-edit.component");
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app',
            templateUrl: 'app/components/app/app.component.html',
            directives: [
                router_deprecated_1.ROUTER_DIRECTIVES,
                header_component_1.HeaderComponent
            ],
            providers: [
                model_service_1.ModelService,
            ]
        }),
        router_deprecated_1.RouteConfig([
            new router_deprecated_1.Route({
                path: '/home',
                name: 'Home',
                component: home_component_1.HomeComponent
            }),
            new router_deprecated_1.Route({
                path: '/edit',
                name: 'Edit',
                component: data_edit_component_1.DataEditComponent
            }),
            new router_deprecated_1.Route({
                path: '/evaluation',
                name: 'Evaluation',
                component: data_evaluation_component_1.DataEvaluationComponent
            }),
            new router_deprecated_1.Route({
                path: '/license',
                name: 'License',
                component: license_component_1.LicenseComponent
            })
        ]), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map
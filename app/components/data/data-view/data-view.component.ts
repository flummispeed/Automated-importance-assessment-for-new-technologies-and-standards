/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {Component, OnChanges, Input, Output, EventEmitter} from '@angular/core';


import {Model} from '../../../class/model/model';
import {ModelService} from '../../../services/model/model.service';
import {SearchTerm} from "../../../class/search/search-term";
import {ModelComponent} from './model/model.component';
import {Result} from "../../../class/result/result";
import {EvaluationDataService} from "../../../services/evaluation-data/evaluation-data.service";
import {SearchService} from "../../../services/search/search.service";
import {EvaluationDataResult} from "../../../services/evaluation-data/evaluation-data-result";
import {ResultList} from "../../../class/result/result-list";
import {SearchDone} from "../../../class/search/search-done";
import {DataViewPresentationChooseComponent} from "./data-view-presentation-choose.component";


/**
 * Component to switch between the data collection and evaluation
 */
@Component ( {
    selector: 'data-view',
    templateUrl: 'app/components/data/data-view/data-view.component.html',
    directives: [ ModelComponent, DataViewPresentationChooseComponent ],
    providers: [ ModelService, EvaluationDataService, SearchService ]
} )

export class DataViewComponent implements OnChanges
{
    model:Model;
    errorMessage:string;

    contentPresentation:string = 'table';

    @Input ()
    term:SearchTerm;

    @Input ()
    state:string;

    @Input ()
    searchDone:SearchDone;

    @Output ( 'stateSet' ) stateSet:EventEmitter<any> = new EventEmitter ();


    result:Result = new Result ();

    resultList:ResultList = new ResultList ();
    storeResultMessage = "";

    loadModel:boolean = true;

    warning:string = '';

    constructor( private modelService:ModelService,
                 private evaluationDataService:EvaluationDataService )
    {
        this.searchDone = new SearchDone;
    }

    ngOnChanges():any
    {
        if ( this.loadModel )
        {
            this.getModel ();
        }
        if ( this.searchDone.id != 0 )
        {
            this.result = new Result ();
            this.getResult ();
        }
    }


    getModel()
    {
        this.modelService.getModel ()
            .subscribe (
                model => this.setModel ( model ),
                error => this.errorMessage = <any>error
            );
    }

    getResult()
    {
        this.evaluationDataService.getResult ( this.searchDone.id )
            .subscribe (
                result => this.setResult ( result ),
                error => this.errorMessage = <any>error
            );
    }

    removeResult( result:Result )
    {
        this.resultList.removeResult ( result.idSearch );
    }

    setResult( result )
    {
        this.result = result;
        if ( this.state == 'Evaluate Data' )
        {
            if(this.resultList.results.length < 4)
            {
                this.resultList.addResult ( result );
            }
            else
            {
                this.warning = 'Maximal 4 results can be compared.';
            }
        }
        
    }

    setStoredResult( result )
    {
        this.result = result;
        this.storeResultMessage = "Data has been stored";
        this.state = "stored";
        this.stateSet.emit ( this.state );
    }

    getStoreResultColor()
    {
        if ( this.storeResultMessage == "Data has been stored" )
        {
            return "rgba(166, 250, 129, 0.5)";
        }
        else
        {
            return "rgba(255, 186, 69, 0.34)";
        }
    }

    getTopic( topicId ):string
    {
        let topic = SearchService.getTopicById ( topicId );
        return topic.topic;
    }

    setModel( model )
    {
        this.model = model;
        this.loadModel = false;
    }

    addData()
    {
        this.evaluationDataService.addData ( this.result ).subscribe (
            result => this.setStoredResult ( result ),
            error => this.storeResultMessage = <any>error.toString ()
        );
    }

    storeResults()
    {
        this.result.term = this.term;
        this.result.resultItems = this.model.getEvaluationValues ();
        this.addData ();
        EvaluationDataResult.result = this.result;

        //TODO to be implemented: routing to evaluation of data
    }

    setContentPresentation(representation)
    {
        this.contentPresentation = representation;
    }

}

/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var model_service_1 = require('../../../services/model/model.service');
var search_term_1 = require("../../../class/search/search-term");
var model_component_1 = require('./model/model.component');
var result_1 = require("../../../class/result/result");
var evaluation_data_service_1 = require("../../../services/evaluation-data/evaluation-data.service");
var search_service_1 = require("../../../services/search/search.service");
var evaluation_data_result_1 = require("../../../services/evaluation-data/evaluation-data-result");
var result_list_1 = require("../../../class/result/result-list");
var search_done_1 = require("../../../class/search/search-done");
var data_view_presentation_choose_component_1 = require("./data-view-presentation-choose.component");
var DataViewComponent = (function () {
    function DataViewComponent(modelService, evaluationDataService) {
        this.modelService = modelService;
        this.evaluationDataService = evaluationDataService;
        this.contentPresentation = 'table';
        this.stateSet = new core_1.EventEmitter();
        this.result = new result_1.Result();
        this.resultList = new result_list_1.ResultList();
        this.storeResultMessage = "";
        this.loadModel = true;
        this.warning = '';
        this.searchDone = new search_done_1.SearchDone;
    }
    DataViewComponent.prototype.ngOnChanges = function () {
        if (this.loadModel) {
            this.getModel();
        }
        if (this.searchDone.id != 0) {
            this.result = new result_1.Result();
            this.getResult();
        }
    };
    DataViewComponent.prototype.getModel = function () {
        var _this = this;
        this.modelService.getModel()
            .subscribe(function (model) { return _this.setModel(model); }, function (error) { return _this.errorMessage = error; });
    };
    DataViewComponent.prototype.getResult = function () {
        var _this = this;
        this.evaluationDataService.getResult(this.searchDone.id)
            .subscribe(function (result) { return _this.setResult(result); }, function (error) { return _this.errorMessage = error; });
    };
    DataViewComponent.prototype.removeResult = function (result) {
        this.resultList.removeResult(result.idSearch);
    };
    DataViewComponent.prototype.setResult = function (result) {
        this.result = result;
        if (this.state == 'Evaluate Data') {
            if (this.resultList.results.length < 4) {
                this.resultList.addResult(result);
            }
            else {
                this.warning = 'Maximal 4 results can be compared.';
            }
        }
    };
    DataViewComponent.prototype.setStoredResult = function (result) {
        this.result = result;
        this.storeResultMessage = "Data has been stored";
        this.state = "stored";
        this.stateSet.emit(this.state);
    };
    DataViewComponent.prototype.getStoreResultColor = function () {
        if (this.storeResultMessage == "Data has been stored") {
            return "rgba(166, 250, 129, 0.5)";
        }
        else {
            return "rgba(255, 186, 69, 0.34)";
        }
    };
    DataViewComponent.prototype.getTopic = function (topicId) {
        var topic = search_service_1.SearchService.getTopicById(topicId);
        return topic.topic;
    };
    DataViewComponent.prototype.setModel = function (model) {
        this.model = model;
        this.loadModel = false;
    };
    DataViewComponent.prototype.addData = function () {
        var _this = this;
        this.evaluationDataService.addData(this.result).subscribe(function (result) { return _this.setStoredResult(result); }, function (error) { return _this.storeResultMessage = error.toString(); });
    };
    DataViewComponent.prototype.storeResults = function () {
        this.result.term = this.term;
        this.result.resultItems = this.model.getEvaluationValues();
        this.addData();
        evaluation_data_result_1.EvaluationDataResult.result = this.result;
        //TODO to be implemented: routing to evaluation of data
    };
    DataViewComponent.prototype.setContentPresentation = function (representation) {
        this.contentPresentation = representation;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', search_term_1.SearchTerm)
    ], DataViewComponent.prototype, "term", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], DataViewComponent.prototype, "state", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', search_done_1.SearchDone)
    ], DataViewComponent.prototype, "searchDone", void 0);
    __decorate([
        core_1.Output('stateSet'), 
        __metadata('design:type', core_1.EventEmitter)
    ], DataViewComponent.prototype, "stateSet", void 0);
    DataViewComponent = __decorate([
        core_1.Component({
            selector: 'data-view',
            templateUrl: 'app/components/data/data-view/data-view.component.html',
            directives: [model_component_1.ModelComponent, data_view_presentation_choose_component_1.DataViewPresentationChooseComponent],
            providers: [model_service_1.ModelService, evaluation_data_service_1.EvaluationDataService, search_service_1.SearchService]
        }), 
        __metadata('design:paramtypes', [model_service_1.ModelService, evaluation_data_service_1.EvaluationDataService])
    ], DataViewComponent);
    return DataViewComponent;
}());
exports.DataViewComponent = DataViewComponent;
//# sourceMappingURL=data-view.component.js.map
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {Component, Output, EventEmitter} from '@angular/core';

/**
 * Component to switch between a tabular and a panel view to present the data sets.
 */
@Component ( {
    selector: 'data-view-presentation-choose',
    templateUrl: 'app/components/data/data-view/data-view-presentation-choose.component.html'
} )

export class DataViewPresentationChooseComponent
{
    @Output ( 'contentPresentation' ) contentPresentation:EventEmitter<string> = new EventEmitter ();

    switchContentPresentation (presentation:string)
    {
        this.contentPresentation.emit(presentation);
    }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
var core_1 = require('@angular/core');
var model_evaluation_1 = require('../../../../../class/model/evaluation/model-evaluation');
var search_term_1 = require("../../../../../class/search/search-term");
var model_evaluation_view_component_1 = require("./model-evaluation-view.component");
var model_evaluation_search_terms_component_1 = require("./model-evaluation-search-terms.component");
var result_list_1 = require("../../../../../class/result/result-list");
var result_1 = require("../../../../../class/result/result");
var model_evaluation_manual_hyperlinklist_1 = require("../../../../../class/model/evaluation/model-evaluation-manual-hyperlinklist");
var ModelEvaluationComponent = (function () {
    function ModelEvaluationComponent() {
        /**
         * To show warnings and information. If this value is not set to "" the string will be displayed.
         * @type {string}
         */
        this.warning = "";
        this.isDataRangeDisplayed = false;
        this.isViewHyperlinkList = false;
    }
    /**
     * Handle preset of values.
     */
    ModelEvaluationComponent.prototype.ngOnChanges = function () {
        var value = this.result.getResultOnId(this.evaluation.id);
        if (value != 'noValue') {
            this.evaluation.value = this.result.getResultItemOnId(this.evaluation.id);
        }
        else {
            this.evaluation.value.value = '';
            this.evaluation.value.valueCanNotBeFound = 'val';
        }
        this.handleObjectValue();
        if (this.evaluation.category) {
            this.evaluation.category.category.selectItemByValue(this.getValue());
        }
        if (this.evaluation.value.value == '' && this.evaluation.manual && this.evaluation.manual.defaultVal != '') {
            this.evaluation.value.value = this.evaluation.manual.defaultVal;
        }
    };
    /**
     * Only accept int values.
     */
    ModelEvaluationComponent.prototype.filterInt = function () {
        this.warning = ''; //reset warning
        var filteredValue = '';
        filteredValue = this.evaluation.value.value.toString().replace(/[^\-0-9]/g, "");
        var minusFound = filteredValue.search(/^-/g);
        if (minusFound != -1) {
            this.evaluation.value.value = "-" + filteredValue.replace(/-/g, "");
        }
        else {
            this.evaluation.value.value = filteredValue.replace(/-/g, "");
        }
        this.evaluation.value.valueCanNotBeFound = 'val';
        if (this.evaluation.value.value == '') {
            this.evaluation.value.valueCanNotBeFound = 'noValA';
        }
    };
    /**
     * Checks whether the value fits to the given minimum, maximum and step size.
     */
    ModelEvaluationComponent.prototype.checkInt = function () {
        var value = this.evaluation.value.value * 1;
        if (value < this.evaluation.manual.min) {
            this.evaluation.value.value = this.evaluation.manual.min + "";
            this.warning = "Value set to defined minimum";
            this.isDataRangeDisplayed = false;
        }
        if (value > this.evaluation.manual.max) {
            this.evaluation.value.value = this.evaluation.manual.max + "";
            this.warning = "Value set to defined maximum";
            this.isDataRangeDisplayed = false;
        }
        var modulo = value % this.evaluation.manual.stepsize;
        if (modulo != 0) {
            var resValue = value - modulo;
            this.evaluation.value.value = resValue + "";
            this.warning = "Input does not fit to step size of " + this.evaluation.manual.stepsize + ".";
            this.isDataRangeDisplayed = false;
        }
    };
    /**
     * Writes the value range in the warning attribute.
     */
    ModelEvaluationComponent.prototype.getManualValueRange = function () {
        if (!this.isDataRangeDisplayed) {
            this.isDataRangeDisplayed = true;
            var range = "Value range: Minimum = '" + this.evaluation.manual.min;
            range += "', Maximum = '" + this.evaluation.manual.max;
            range += "', Default = '" + this.evaluation.manual.defaultVal;
            range += "', Step size = '" + this.evaluation.manual.stepsize;
            this.warning = range;
        }
        else {
            this.isDataRangeDisplayed = false;
            this.warning = '';
        }
    };
    /**
     * Get value of loaded result.
     * @param result
     * @returns {any}
     */
    ModelEvaluationComponent.prototype.getValue = function (result) {
        var value;
        if (result) {
            var resultItem = result.getResultItemOnId(this.evaluation.id);
            if (resultItem.valueCanNotBeFound == 'noValA') {
                value = 'n.a.';
            }
            else {
                value = resultItem.value;
            }
        }
        else {
            value = this.result.getResultOnId(this.evaluation.id);
        }
        if (value != 'noValue') {
            return value;
        }
        else {
            if (this.evaluation.manual) {
                return this.evaluation.manual.defaultVal;
            }
        }
        return '';
    };
    ModelEvaluationComponent.prototype.getHyperlinkList = function (result) {
        var resultItem = result.getResultItemOnId(this.evaluation.id);
        return resultItem.value.urls;
    };
    /**
     * Check if result list is empty.
     * @returns {boolean}
     */
    ModelEvaluationComponent.prototype.isEmptyResultList = function () {
        if (this.resultList.listLength() < 1) {
            return true;
        }
        return false;
    };
    /**
     * Set the evaluation value to the item.
     * @param item
     */
    ModelEvaluationComponent.prototype.chooseCategory = function (item) {
        this.evaluation.value.value = item.value;
        this.evaluation.value.valueCanNotBeFound = 'val';
        this.evaluation.category.category.selectItem(item);
    };
    /**
     * Set the valueCanNotBeFound state of the result item.
     */
    ModelEvaluationComponent.prototype.handleValueCanNotBeFound = function () {
        if (this.evaluation.value.valueCanNotBeFound == 'noValA') {
            this.warning = 'Please insert or choose a value. Otherwise it will be stored as no value can be found.';
        }
        if (this.evaluation.value.valueCanNotBeFound == 'val') {
            this.evaluation.value.valueCanNotBeFound = 'noValA';
            this.evaluation.value.value = '';
        }
    };
    /**
     * Category can not be found.
     */
    ModelEvaluationComponent.prototype.handleCategoryValueCanNotBeFound = function () {
        this.handleValueCanNotBeFound();
        this.evaluation.value.value = '';
        this.evaluation.category.category.deselectAllItems();
    };
    ModelEvaluationComponent.prototype.handleObjectValue = function () {
        if (this.evaluation.value.type == 'hyperlinkList') {
            this.evaluation.manual.hyperlinkList = new model_evaluation_manual_hyperlinklist_1.ModelEvaluationManualHyperlinkList();
            if (this.evaluation.value.value.hasOwnProperty('urls')) {
                for (var _i = 0, _a = this.evaluation.value.value.urls; _i < _a.length; _i++) {
                    var urlItem = _a[_i];
                    this.evaluation.manual.hyperlinkList.urls.push(urlItem);
                }
            }
        }
    };
    ModelEvaluationComponent.prototype.toggleViewHyperlinklist = function () {
        this.isViewHyperlinkList = !this.isViewHyperlinkList;
    };
    ModelEvaluationComponent.prototype.isHyperlinkList = function (result) {
        if (this.evaluation.value.type == 'hyperlinkList') {
            if (this.getHyperlinkList(result).length > 0) {
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', model_evaluation_1.ModelEvaluation)
    ], ModelEvaluationComponent.prototype, "evaluation", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', search_term_1.SearchTerm)
    ], ModelEvaluationComponent.prototype, "term", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', result_list_1.ResultList)
    ], ModelEvaluationComponent.prototype, "resultList", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], ModelEvaluationComponent.prototype, "state", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', result_1.Result)
    ], ModelEvaluationComponent.prototype, "result", void 0);
    ModelEvaluationComponent = __decorate([
        core_1.Component({
            selector: 'model-evaluation',
            templateUrl: 'app/components/data/data-view/model/evaluation/model-evaluation.component.html',
            directives: [model_evaluation_view_component_1.ModelEvaluationViewComponent, model_evaluation_search_terms_component_1.ModelEvaluationSearchTermComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], ModelEvaluationComponent);
    return ModelEvaluationComponent;
}());
exports.ModelEvaluationComponent = ModelEvaluationComponent;
//# sourceMappingURL=model-evaluation.component.js.map
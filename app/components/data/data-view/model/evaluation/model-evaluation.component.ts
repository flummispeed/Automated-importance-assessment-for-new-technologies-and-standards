/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
import {Component, Input, OnChanges} from '@angular/core';
import {ModelEvaluation} from '../../../../../class/model/evaluation/model-evaluation';
import {SearchTerm} from "../../../../../class/search/search-term";
import {CategoryItem} from "../../../../../class/category/category";
import {ModelEvaluationViewComponent} from "./model-evaluation-view.component";
import {ModelEvaluationSearchTermComponent} from "./model-evaluation-search-terms.component";
import {ResultList} from "../../../../../class/result/result-list";
import {Result} from "../../../../../class/result/result";
import {ModelEvaluationManualHyperlinkList} from "../../../../../class/model/evaluation/model-evaluation-manual-hyperlinklist";

@Component ( {
    selector: 'model-evaluation',
    templateUrl: 'app/components/data/data-view/model/evaluation/model-evaluation.component.html',
    directives: [ ModelEvaluationViewComponent, ModelEvaluationSearchTermComponent ]
} )

export class ModelEvaluationComponent implements OnChanges
{
    @Input ()
    evaluation:ModelEvaluation;
    @Input ()
    term:SearchTerm;
    @Input ()
    resultList:ResultList;
    @Input ()
    state:string;
    @Input ()
    result:Result;

    /**
     * To show warnings and information. If this value is not set to "" the string will be displayed.
     * @type {string}
     */
    warning:string = "";

    isDataRangeDisplayed:boolean = false;

    isViewHyperlinkList:boolean = false;

    /**
     * Handle preset of values.
     */
    ngOnChanges():any
    {
        let value = this.result.getResultOnId ( this.evaluation.id );
        if ( value != 'noValue' )
        {
            this.evaluation.value = this.result.getResultItemOnId ( this.evaluation.id );
        }
        else
        {
            this.evaluation.value.value = '';
            this.evaluation.value.valueCanNotBeFound = 'val';
        }
        this.handleObjectValue ();
        if ( this.evaluation.category )
        {
            this.evaluation.category.category.selectItemByValue ( this.getValue () );
        }
        if ( this.evaluation.value.value == '' && this.evaluation.manual && this.evaluation.manual.defaultVal != '' )
        {
            this.evaluation.value.value = this.evaluation.manual.defaultVal;
        }
    }

    /**
     * Only accept int values.
     */
    filterInt()
    {
        this.warning = ''; //reset warning
        let filteredValue = '';
        filteredValue = this.evaluation.value.value.toString ().replace ( /[^\-0-9]/g, "" );
        let minusFound = filteredValue.search ( /^-/g );
        if ( minusFound != -1 )
        {
            this.evaluation.value.value = "-" + filteredValue.replace ( /-/g, "" );
        }
        else
        {
            this.evaluation.value.value = filteredValue.replace ( /-/g, "" );
        }
        this.evaluation.value.valueCanNotBeFound = 'val';
        if ( this.evaluation.value.value == '' )
        {
            this.evaluation.value.valueCanNotBeFound = 'noValA';
        }
    }

    /**
     * Checks whether the value fits to the given minimum, maximum and step size.
     */
    checkInt()
    {
        let value = this.evaluation.value.value * 1;
        if ( value < this.evaluation.manual.min )
        {
            this.evaluation.value.value = this.evaluation.manual.min + "";
            this.warning = "Value set to defined minimum";
            this.isDataRangeDisplayed = false;
        }
        if ( value > this.evaluation.manual.max )
        {
            this.evaluation.value.value = this.evaluation.manual.max + "";
            this.warning = "Value set to defined maximum";
            this.isDataRangeDisplayed = false;
        }
        let modulo = value % this.evaluation.manual.stepsize;
        if ( modulo != 0 )
        {
            let resValue = value - modulo;
            this.evaluation.value.value = resValue + "";
            this.warning = "Input does not fit to step size of " + this.evaluation.manual.stepsize + ".";
            this.isDataRangeDisplayed = false;
        }
    }

    /**
     * Writes the value range in the warning attribute.
     */
    getManualValueRange()
    {
        if ( !this.isDataRangeDisplayed )
        {
            this.isDataRangeDisplayed = true;
            let range = "Value range: Minimum = '" + this.evaluation.manual.min;
            range += "', Maximum = '" + this.evaluation.manual.max;
            range += "', Default = '" + this.evaluation.manual.defaultVal;
            range += "', Step size = '" + this.evaluation.manual.stepsize;
            this.warning = range;
        }
        else
        {
            this.isDataRangeDisplayed = false;
            this.warning = '';
        }

    }

    /**
     * Get value of loaded result.
     * @param result
     * @returns {any}
     */
    getValue( result?:Result )
    {

        let value;
        if ( result )
        {
            let resultItem = result.getResultItemOnId ( this.evaluation.id );
            if ( resultItem.valueCanNotBeFound == 'noValA' )
            {
                value = 'n.a.'
            }
            else
            {
                value = resultItem.value;
            }
        }
        else
        {
            value = this.result.getResultOnId ( this.evaluation.id );
        }
        if ( value != 'noValue' )
        {
            return value;
        }
        else
        {
            if ( this.evaluation.manual )
            {
                return this.evaluation.manual.defaultVal;
            }
        }
        return '';
    }

    getHyperlinkList(result?:Result)
    {
        let resultItem = result.getResultItemOnId ( this.evaluation.id );
        return resultItem.value.urls;
    }

    /**
     * Check if result list is empty.
     * @returns {boolean}
     */
    isEmptyResultList()
    {
        if ( this.resultList.listLength () < 1 )
        {
            return true;
        }
        return false;
    }

    /**
     * Set the evaluation value to the item.
     * @param item
     */
    chooseCategory( item:CategoryItem )
    {
        this.evaluation.value.value = item.value;
        this.evaluation.value.valueCanNotBeFound = 'val';
        this.evaluation.category.category.selectItem ( item );
    }

    /**
     * Set the valueCanNotBeFound state of the result item.
     */
    handleValueCanNotBeFound()
    {
        if ( this.evaluation.value.valueCanNotBeFound == 'noValA' )
        {
            this.warning = 'Please insert or choose a value. Otherwise it will be stored as no value can be found.';
        }
        if ( this.evaluation.value.valueCanNotBeFound == 'val' )
        {
            this.evaluation.value.valueCanNotBeFound = 'noValA';
            this.evaluation.value.value = '';
        }
    }

    /**
     * Category can not be found.
     */
    handleCategoryValueCanNotBeFound()
    {
        this.handleValueCanNotBeFound ();
        this.evaluation.value.value = '';
        this.evaluation.category.category.deselectAllItems ();
    }

    handleObjectValue()
    {
        if ( this.evaluation.value.type == 'hyperlinkList' )
        {
            this.evaluation.manual.hyperlinkList = new ModelEvaluationManualHyperlinkList ();
            if ( this.evaluation.value.value.hasOwnProperty ( 'urls' ) )
            {
                for ( var urlItem of this.evaluation.value.value.urls )
                {
                    this.evaluation.manual.hyperlinkList.urls.push ( urlItem );
                }
            }
        }

    }

    toggleViewHyperlinklist()
    {
        this.isViewHyperlinkList = !this.isViewHyperlinkList;
    }

    isHyperlinkList(result:Result)
    {
        if(this.evaluation.value.type == 'hyperlinkList')
        {
            if(this.getHyperlinkList(result).length > 0)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        return false;
    }

}
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
import {Component, Input} from '@angular/core';
import {SearchTerm} from "../../../../../class/search/search-term";
import {ModelEvaluationSearchTerms} from "../../../../../class/model/evaluation/model-evaluation-search-terms";

@Component ( {
    selector: 'model-evaluation-search-term',
    templateUrl: 'app/components/data/data-view/model/evaluation/model-evaluation-search-terms.component.html'
} )

export class ModelEvaluationSearchTermComponent
{
    @Input ()
    term:SearchTerm;

    @Input ()
    searchTerms:ModelEvaluationSearchTerms[];

    /**
     * Compile a hyperlink to google search.
     * @param searchTerm
     * @param fuzzy
     * @returns {string}
     */
    getGoogleLink( searchTerm, fuzzy?:boolean ):string
    {
        let url:string = 'https://www.google.com/search?';
        url += 'num=' + searchTerm.numberOfResults;
        // Surrounding quotes will not be used in case of fuzzy search.
        if ( fuzzy )
        {
            url += '&q=' + this.term.getGoogleSearchTerm () ;
        }
        else
        {
            url += '&as_epq=%22' + this.term.getGoogleSearchTerm () + '%22';
        }
        url += '&as_q='
        return url + this.getSpecifiedQueryTerm ( searchTerm );
    }

    /**
     * Compile the specified query term.
     * @param searchTerm
     * @returns {string}
     */
    getSpecifiedQueryTerm( searchTerm )
    {
        let searchParameter:string = '';
        let i = 1;
        for ( var term of searchTerm.searchTerms )
        {
            if ( i > 1 )
            {
                searchParameter += 'OR'
            }
            i = i + 1;
            searchParameter += '%22';
            searchParameter += term.replace ( ' ', '+' );
            searchParameter += '%22';
        }
        return searchParameter;
    }

    /**
     * Replace pattern ###term### with the given term to be evaluated and
     * return the description of the ModelEvaluationSearchTerms
     * @param searchTerm
     * @returns {string}
     */
    getDescription( searchTerm:ModelEvaluationSearchTerms )
    {
        return searchTerm.taskDescription.replace ( '###term###', "'" + this.term.term + "'" );
    }

}
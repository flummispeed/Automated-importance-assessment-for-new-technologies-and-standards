/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {Component, Input} from '@angular/core';

import {ModelEvaluationView} from '../../../../../class/model/evaluation/model-evaluation-view';
import {SearchTerm} from "../../../../../class/search/search-term";

@Component ( {
    selector: 'model-evaluation-view',
    templateUrl: 'app/components/data/data-view/model/evaluation/model-evaluation-view.component.html'
} )

export class ModelEvaluationViewComponent
{
    @Input()
    views:ModelEvaluationView[];

    @Input ()
    term:SearchTerm;

    getUrl(view:ModelEvaluationView)
    {
        let term = this.term.term.replace(' ', '%20');
        term = view.queryQuotes + term + view.queryQuotes;
        return view.url + view.urlParam + term;
    }

    getDescription(view:ModelEvaluationView)
    {
        return view.description.replace('###term###', "'" + this.term.term + "'");
    }
}
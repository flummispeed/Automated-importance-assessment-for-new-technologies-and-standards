"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
var core_1 = require('@angular/core');
var model_category_1 = require('../../../../class/model/model-category');
var model_field_component_1 = require("./model-field.component");
var search_term_1 = require("../../../../class/search/search-term");
var result_list_1 = require("../../../../class/result/result-list");
var result_1 = require("../../../../class/result/result");
var ModelCategoryComponent = (function () {
    function ModelCategoryComponent() {
        /**
         * Flag for switching chevron symbol.
         * @type {boolean}
         */
        this.chevronUp = false;
        this.help = '';
    }
    /**
     * Switch between chevron up and down.
     */
    ModelCategoryComponent.prototype.changeChevron = function () {
        if (this.chevronUp) {
            this.chevronUp = false;
        }
        else {
            this.chevronUp = true;
        }
    };
    ModelCategoryComponent.prototype.getEvaluationStateStyle = function () {
        var style = '#ddd';
        if (this.modelCategory.isEvaluated() && this.state == "Collect Data") {
            style = 'rgba(166, 250, 129, 0.5)';
        }
        return style;
    };
    ModelCategoryComponent.prototype.getHelp = function () {
        var help = '';
        if (this.modelCategory.help) {
            help = this.modelCategory.help.manual;
            for (var _i = 0, _a = this.modelCategory.help.url; _i < _a.length; _i++) {
                var url = _a[_i];
                help = help + '\n' + url;
            }
        }
        if (this.help == '') {
            this.help = help;
        }
        else {
            this.help = '';
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', model_category_1.ModelCategory)
    ], ModelCategoryComponent.prototype, "modelCategory", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', search_term_1.SearchTerm)
    ], ModelCategoryComponent.prototype, "term", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', result_list_1.ResultList)
    ], ModelCategoryComponent.prototype, "resultList", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], ModelCategoryComponent.prototype, "contentPresentation", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], ModelCategoryComponent.prototype, "level", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], ModelCategoryComponent.prototype, "state", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', result_1.Result)
    ], ModelCategoryComponent.prototype, "result", void 0);
    ModelCategoryComponent = __decorate([
        core_1.Component({
            selector: 'model-category',
            templateUrl: 'app/components/data/data-view/model/model-category.component.html',
            //  styleUrls: ['app/components/model/model-category.component.html'],
            directives: [ModelCategoryComponent, model_field_component_1.ModelFieldComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], ModelCategoryComponent);
    return ModelCategoryComponent;
}());
exports.ModelCategoryComponent = ModelCategoryComponent;
//# sourceMappingURL=model-category.component.js.map
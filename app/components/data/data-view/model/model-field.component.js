"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
var core_1 = require('@angular/core');
var model_field_1 = require('../../../../class/model/model-field');
var search_term_1 = require("../../../../class/search/search-term");
var model_evaluation_component_1 = require('./evaluation/model-evaluation.component');
var result_list_1 = require("../../../../class/result/result-list");
var result_1 = require("../../../../class/result/result");
var ModelFieldComponent = (function () {
    function ModelFieldComponent() {
        this.manual = '';
    }
    ModelFieldComponent.prototype.showManual = function () {
        if (this.manual == '') {
            this.manual = this.modelField.manual.manual;
            for (var _i = 0, _a = this.modelField.manual.url; _i < _a.length; _i++) {
                var url = _a[_i];
                this.manual = this.manual + ' ' + url;
            }
        }
        else {
            this.manual = '';
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', model_field_1.ModelField)
    ], ModelFieldComponent.prototype, "modelField", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', search_term_1.SearchTerm)
    ], ModelFieldComponent.prototype, "term", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', result_list_1.ResultList)
    ], ModelFieldComponent.prototype, "resultList", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], ModelFieldComponent.prototype, "state", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', result_1.Result)
    ], ModelFieldComponent.prototype, "result", void 0);
    ModelFieldComponent = __decorate([
        core_1.Component({
            selector: 'model-field',
            templateUrl: 'app/components/data/data-view/model/model-field.component.html',
            directives: [model_evaluation_component_1.ModelEvaluationComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], ModelFieldComponent);
    return ModelFieldComponent;
}());
exports.ModelFieldComponent = ModelFieldComponent;
//# sourceMappingURL=model-field.component.js.map
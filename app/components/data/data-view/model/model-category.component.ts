/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
import {Component, Input} from '@angular/core';
import {ModelCategory} from '../../../../class/model/model-category';
import {ModelFieldComponent} from "./model-field.component";
import {SearchTerm} from "../../../../class/search/search-term";
import {ResultList} from "../../../../class/result/result-list";
import {Result} from "../../../../class/result/result";

@Component ( {
    selector: 'model-category',
    templateUrl: 'app/components/data/data-view/model/model-category.component.html',
    //  styleUrls: ['app/components/model/model-category.component.html'],
    directives: [ ModelCategoryComponent, ModelFieldComponent ]
} )

export class ModelCategoryComponent
{
    @Input () modelCategory:ModelCategory;
    @Input () term:SearchTerm;
    @Input () resultList:ResultList;
    @Input () contentPresentation:string;
    @Input () level:number;
    @Input () state:string;
    @Input () result:Result;

    /**
     * Flag for switching chevron symbol.
     * @type {boolean}
     */
    chevronUp:boolean = false;

    help:string = '';

    /**
     * Switch between chevron up and down.
     */
    changeChevron()
    {
        if ( this.chevronUp )
        {
            this.chevronUp = false;
        }
        else
        {
            this.chevronUp = true;
        }
    }

    getEvaluationStateStyle()
    {
        let style = '#ddd';
        if ( this.modelCategory.isEvaluated () && this.state == "Collect Data" )
        {
            style = 'rgba(166, 250, 129, 0.5)';
        }
        return style;
    }

    getHelp()
    {
        let help:string = '';
        if ( this.modelCategory.help )
        {
            help = this.modelCategory.help.manual;
            for ( var url of this.modelCategory.help.url )
            {
                help = help + '\n' + url;
            }
        }
        if(this.help == '')
        {
            this.help = help;
        }
        else
        {
            this.help = '';
        }
    }

}
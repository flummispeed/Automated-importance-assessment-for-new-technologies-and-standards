/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
import {Component, Input, Output, EventEmitter} from '@angular/core';
import {SearchService} from "../../../../services/search/search.service";
import {SearchValue} from "../../../../class/search/search-value";
import {SearchTopic} from "../../../../class/search/search-topic";
import {SearchTerm} from "../../../../class/search/search-term";

@Component ( {
    selector: 'search-add',
    templateUrl: 'app/components/data/search/search-add/search-add.component.html',
    providers: [ SearchService ]
} )

/**
 * Class SearchAddComponent
 * Add a new topic or term.
 */
export class SearchAddComponent
{
    /**
     * Either this will be a new topic or an existing topic. In case of a new topic a topic will be added first
     * and afterwards a new term that belongs to the new topic. In case of an existing topic a new term that will be added.
     */
    @Input ()
    topic:SearchTopic;

    /**
     * Emits the new or selected term.
     * @type {EventEmitter}
     */
    @Output ()
    selectTerm:EventEmitter<any> = new EventEmitter ();

    @Output ( 'abortSearchAdd' )
    abortSearchAdd:EventEmitter<boolean> = new EventEmitter ();

    /**
     * List of suggestions to support the user.
     * Shows the user already stored topics and terms.
     * @type {Array}
     */
    public autoFills:SearchValue[] = [];

    /**
     * List of the SearchValue values of the autoFills list.
     * @type {Array}
     */
    public list:string[] = [];

    /**
     *
     * @type {Array}
     */
    public filteredList:string[] = [];

    /**
     * Term inserted by the user
     * @type {string}
     */
    public query = '';

    /**
     * Warnings to be displayed.
     * @type {string}
     */
    public warning:string = '';

    //TODO implement error handling
    public errorMessage:string;

    constructor( private searchService:SearchService )
    {
        //nothing more to do here
    }

    /**
     * Load auto fill
     */
    startAutoFill()
    {
        if ( this.topic.id == 0 )
        {
            this.getAutoFill ();
        }
        else
        {
            this.getAutoFill ( "term", this.topic.id );
        }
    }

    /**
     * Try to get an auto fill list to support the user.
     * @param table
     * @param foreignKey
     */
    getAutoFill( table?:string, foreignKey?:number )
    {
        this.searchService.getAutoFill ( table, foreignKey ).subscribe (
            autoFills => this.fillList ( autoFills ),
            error => this.errorMessage = <any>error );
    }

    /**
     * Fill the list with the values of the SearchValue items of the autoFills list.
     */
    fillList( autoFills:SearchValue[] )
    {
        this.autoFills = autoFills;
        this.list = []; // clear list to get the actual state of the list.
        for ( var item of this.autoFills )
        {
            this.list.push ( item.value );
        }
    }

    /**
     * Filter the auto fill list.
     */
    filter()
    {
        this.warning = '';
        if ( this.query !== "" )
        {
            this.filteredList = this.list.filter ( function ( el )
            {
                return el.toLowerCase ().indexOf ( this.query.toLowerCase () ) > -1;
            }.bind ( this ) );
        }
        else
        {
            this.filteredList = [];
        }
    }

    /**
     * Select an item of the auto fill list.
     * @param item
     */
    select( item )
    {
        this.query = item;
        this.choose ();
    }

    /**
     * Return the id of the query term.
     * @returns {number}
     *  0 in case no SearchValue fit to the query.
     */
    getSearchId()
    {
        for ( var item of this.autoFills )
        {
            if ( item.value == this.query )
            {
                return item.id;
            }
        }
        return 0;
    }

    /**
     * Switch between chooseTopic() and chooseTerm() to select a search topic and a search term.
     */
    choose()
    {
        if ( this.query == '' )
        {
            this.warning = 'Space character is not allowed';
        }
        else if ( this.query.length < 3 )
        {
            this.warning = 'At least 3 chars are required';
        }
        else
        {
            if ( this.topic.topic == '' )
            {
                this.chooseTopic ();
            }
            else
            {
                this.chooseTerm ();
            }
        }
    }

    /**
     * Checks whether the topic is stored in the database and clears the query.
     * If the topic is new it will be stored in the database. Otherwise the existing topic will be used.
     */
    chooseTopic()
    {
        this.topic = new SearchTopic ();
        this.topic.id = this.getSearchId ();

        this.topic.topic = this.query;
        this.list = []; //Clear auto fill list.
        this.filteredList = []; //Clear auto fill list.
        if ( this.topic.id == 0 ) // new topic
        {
            this.addSearchTopic ();
            // If a topic is new it does not have terms that belongs to it.
            // Therefore the list of auto fill values is cleared.
            this.autoFills = [];
        }
        else
        {
            this.getAutoFill ( "term", this.topic.id );
        }

        this.query = '';
    }

    /**
     * Adds a search topic to database.
     */
    addSearchTopic()
    {
        this.topic.model = 1;

        this.searchService.addSearchTopic ( this.topic )
            .subscribe (
                searchTopic => this.topic = searchTopic,
                error => this.errorMessage = <any>error
            );
    }

    /**
     * Checks whether the term is stored in the database and clears the query.
     */
    chooseTerm()
    {
        let term = new SearchTerm ();
        term.id = this.getSearchId ();

        term.term = this.query;
        term.searchTopicId = this.topic.id;
        this.list = []; //Clear auto fill list.
        this.filteredList = []; //Clear auto fill list.
        this.query = '';
        if ( term.id == 0 )
        {
            if ( this.topic && this.topic.id != 0 )
            {
                this.addSearchTerm ( term );
                this.autoFills = [];
            }
            else
            {
                alert ( 'Sorry, a problem with the server occurred. The data can not be stored. Please try again later.' );
            }
        }
        else
        {
            this.selectTerm.emit ( term );
        }
    }

    /**
     * Adds a search term to database.
     */
    addSearchTerm( term:SearchTerm )
    {
        this.searchService.addSearchTerm ( term )
            .subscribe (
                searchTerm => this.outputTerm ( searchTerm ),
                error => this.errorMessage = <any>error
            );
    }

    outputTerm( term:SearchTerm )
    {
        this.autoFills = [];
        this.selectTerm.emit ( term );
    }

    abortAdd()
    {
        this.autoFills = [];
        this.abortSearchAdd.emit ( true );
    }


}
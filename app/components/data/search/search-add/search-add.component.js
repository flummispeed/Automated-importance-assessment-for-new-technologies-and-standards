"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
var core_1 = require('@angular/core');
var search_service_1 = require("../../../../services/search/search.service");
var search_topic_1 = require("../../../../class/search/search-topic");
var search_term_1 = require("../../../../class/search/search-term");
var SearchAddComponent = (function () {
    function SearchAddComponent(searchService) {
        this.searchService = searchService;
        /**
         * Emits the new or selected term.
         * @type {EventEmitter}
         */
        this.selectTerm = new core_1.EventEmitter();
        this.abortSearchAdd = new core_1.EventEmitter();
        /**
         * List of suggestions to support the user.
         * Shows the user already stored topics and terms.
         * @type {Array}
         */
        this.autoFills = [];
        /**
         * List of the SearchValue values of the autoFills list.
         * @type {Array}
         */
        this.list = [];
        /**
         *
         * @type {Array}
         */
        this.filteredList = [];
        /**
         * Term inserted by the user
         * @type {string}
         */
        this.query = '';
        /**
         * Warnings to be displayed.
         * @type {string}
         */
        this.warning = '';
        //nothing more to do here
    }
    /**
     * Load auto fill
     */
    SearchAddComponent.prototype.startAutoFill = function () {
        if (this.topic.id == 0) {
            this.getAutoFill();
        }
        else {
            this.getAutoFill("term", this.topic.id);
        }
    };
    /**
     * Try to get an auto fill list to support the user.
     * @param table
     * @param foreignKey
     */
    SearchAddComponent.prototype.getAutoFill = function (table, foreignKey) {
        var _this = this;
        this.searchService.getAutoFill(table, foreignKey).subscribe(function (autoFills) { return _this.fillList(autoFills); }, function (error) { return _this.errorMessage = error; });
    };
    /**
     * Fill the list with the values of the SearchValue items of the autoFills list.
     */
    SearchAddComponent.prototype.fillList = function (autoFills) {
        this.autoFills = autoFills;
        this.list = []; // clear list to get the actual state of the list.
        for (var _i = 0, _a = this.autoFills; _i < _a.length; _i++) {
            var item = _a[_i];
            this.list.push(item.value);
        }
    };
    /**
     * Filter the auto fill list.
     */
    SearchAddComponent.prototype.filter = function () {
        this.warning = '';
        if (this.query !== "") {
            this.filteredList = this.list.filter(function (el) {
                return el.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
            }.bind(this));
        }
        else {
            this.filteredList = [];
        }
    };
    /**
     * Select an item of the auto fill list.
     * @param item
     */
    SearchAddComponent.prototype.select = function (item) {
        this.query = item;
        this.choose();
    };
    /**
     * Return the id of the query term.
     * @returns {number}
     *  0 in case no SearchValue fit to the query.
     */
    SearchAddComponent.prototype.getSearchId = function () {
        for (var _i = 0, _a = this.autoFills; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.value == this.query) {
                return item.id;
            }
        }
        return 0;
    };
    /**
     * Switch between chooseTopic() and chooseTerm() to select a search topic and a search term.
     */
    SearchAddComponent.prototype.choose = function () {
        if (this.query == '') {
            this.warning = 'Space character is not allowed';
        }
        else if (this.query.length < 3) {
            this.warning = 'At least 3 chars are required';
        }
        else {
            if (this.topic.topic == '') {
                this.chooseTopic();
            }
            else {
                this.chooseTerm();
            }
        }
    };
    /**
     * Checks whether the topic is stored in the database and clears the query.
     * If the topic is new it will be stored in the database. Otherwise the existing topic will be used.
     */
    SearchAddComponent.prototype.chooseTopic = function () {
        this.topic = new search_topic_1.SearchTopic();
        this.topic.id = this.getSearchId();
        this.topic.topic = this.query;
        this.list = []; //Clear auto fill list.
        this.filteredList = []; //Clear auto fill list.
        if (this.topic.id == 0) {
            this.addSearchTopic();
            // If a topic is new it does not have terms that belongs to it.
            // Therefore the list of auto fill values is cleared.
            this.autoFills = [];
        }
        else {
            this.getAutoFill("term", this.topic.id);
        }
        this.query = '';
    };
    /**
     * Adds a search topic to database.
     */
    SearchAddComponent.prototype.addSearchTopic = function () {
        var _this = this;
        this.topic.model = 1;
        this.searchService.addSearchTopic(this.topic)
            .subscribe(function (searchTopic) { return _this.topic = searchTopic; }, function (error) { return _this.errorMessage = error; });
    };
    /**
     * Checks whether the term is stored in the database and clears the query.
     */
    SearchAddComponent.prototype.chooseTerm = function () {
        var term = new search_term_1.SearchTerm();
        term.id = this.getSearchId();
        term.term = this.query;
        term.searchTopicId = this.topic.id;
        this.list = []; //Clear auto fill list.
        this.filteredList = []; //Clear auto fill list.
        this.query = '';
        if (term.id == 0) {
            if (this.topic && this.topic.id != 0) {
                this.addSearchTerm(term);
                this.autoFills = [];
            }
            else {
                alert('Sorry, a problem with the server occurred. The data can not be stored. Please try again later.');
            }
        }
        else {
            this.selectTerm.emit(term);
        }
    };
    /**
     * Adds a search term to database.
     */
    SearchAddComponent.prototype.addSearchTerm = function (term) {
        var _this = this;
        this.searchService.addSearchTerm(term)
            .subscribe(function (searchTerm) { return _this.outputTerm(searchTerm); }, function (error) { return _this.errorMessage = error; });
    };
    SearchAddComponent.prototype.outputTerm = function (term) {
        this.autoFills = [];
        this.selectTerm.emit(term);
    };
    SearchAddComponent.prototype.abortAdd = function () {
        this.autoFills = [];
        this.abortSearchAdd.emit(true);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', search_topic_1.SearchTopic)
    ], SearchAddComponent.prototype, "topic", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], SearchAddComponent.prototype, "selectTerm", void 0);
    __decorate([
        core_1.Output('abortSearchAdd'), 
        __metadata('design:type', core_1.EventEmitter)
    ], SearchAddComponent.prototype, "abortSearchAdd", void 0);
    SearchAddComponent = __decorate([
        core_1.Component({
            selector: 'search-add',
            templateUrl: 'app/components/data/search/search-add/search-add.component.html',
            providers: [search_service_1.SearchService]
        }), 
        __metadata('design:paramtypes', [search_service_1.SearchService])
    ], SearchAddComponent);
    return SearchAddComponent;
}());
exports.SearchAddComponent = SearchAddComponent;
//# sourceMappingURL=search-add.component.js.map
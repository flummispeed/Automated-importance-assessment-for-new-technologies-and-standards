/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var search_service_1 = require('../../../../services/search/search.service');
var search_topic_1 = require("../../../../class/search/search-topic");
var search_done_1 = require("../../../../class/search/search-done");
var search_add_component_1 = require("../search-add/search-add.component");
var collapse_1 = require("../../../../directives/collapse");
var SearchHistoryComponent = (function () {
    function SearchHistoryComponent(searchService) {
        this.searchService = searchService;
        this.selectedSearch = new core_1.EventEmitter();
        this.searchTermSelected = new core_1.EventEmitter();
        this.searchHistory = [];
        this.isCollapsedAvailableData = false;
        /**
         * view search-add
         */
        this.isSearchAdd = false;
        this.topic = new search_topic_1.SearchTopic();
    }
    SearchHistoryComponent.prototype.ngOnChanges = function () {
        this.getSearchHistory();
    };
    SearchHistoryComponent.prototype.getSearchHistory = function () {
        var _this = this;
        this.searchService.getSearchHistory().subscribe(function (topics) { return _this.searchHistory = topics; }, function (error) { return _this.errorMessage = error; });
    };
    SearchHistoryComponent.prototype.selectSearch = function (item, term) {
        this.isCollapsedAvailableData = !this.isCollapsedAvailableData;
        this.getSearchHistory();
        this.selectedSearch.emit(item);
        if (term) {
            this.searchTermSelected.emit(term);
        }
        else {
            this.searchTermSelected.emit(this.searchService.getTermById(item.searchTermId));
        }
    };
    SearchHistoryComponent.prototype.addSearch = function (topic) {
        if (topic) {
            this.topic = topic;
        }
        else {
            this.topic = new search_topic_1.SearchTopic();
        }
        this.isSearchAdd = true;
    };
    SearchHistoryComponent.prototype.navigateBackToHistory = function () {
        this.isSearchAdd = false;
    };
    SearchHistoryComponent.prototype.selectNewTerm = function (term) {
        this.isSearchAdd = false;
        this.addSearchDone(term);
    };
    SearchHistoryComponent.prototype.addSearchDone = function (term) {
        var _this = this;
        var searchDone = new search_done_1.SearchDone();
        searchDone.searchTermId = term.id;
        this.searchService.addSearchDone(searchDone).subscribe(function (searchDone) { return _this.selectSearch(searchDone, term); }, function (error) { return _this.errorMessage = error; });
    };
    SearchHistoryComponent.prototype.getCollapseButtonText = function () {
        if (!this.isCollapsedAvailableData) {
            return 'hide available data';
        }
        return 'show available data';
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], SearchHistoryComponent.prototype, "selectedSearch", void 0);
    __decorate([
        core_1.Output('searchTermSelected'), 
        __metadata('design:type', core_1.EventEmitter)
    ], SearchHistoryComponent.prototype, "searchTermSelected", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], SearchHistoryComponent.prototype, "searchHistoryType", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], SearchHistoryComponent.prototype, "state", void 0);
    SearchHistoryComponent = __decorate([
        core_1.Component({
            selector: 'search-history',
            templateUrl: 'app/components/data/search/search-history/search-history.component.html',
            directives: [search_add_component_1.SearchAddComponent, collapse_1.Collapse],
            providers: [search_service_1.SearchService]
        }), 
        __metadata('design:paramtypes', [search_service_1.SearchService])
    ], SearchHistoryComponent);
    return SearchHistoryComponent;
}());
exports.SearchHistoryComponent = SearchHistoryComponent;
//# sourceMappingURL=search-history.component.js.map
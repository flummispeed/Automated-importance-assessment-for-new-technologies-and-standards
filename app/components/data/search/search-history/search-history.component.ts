/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {Component, OnChanges, Input, Output, EventEmitter} from '@angular/core';
import {SearchService} from '../../../../services/search/search.service';
import {SearchTopic} from "../../../../class/search/search-topic";
import {SearchDone} from "../../../../class/search/search-done";
import {SearchAddComponent} from "../search-add/search-add.component";
import {SearchTerm} from "../../../../class/search/search-term";
import {Collapse} from "../../../../directives/collapse";

/**
 * Component to describe the topics there associated terms and the searches that have been done associated to the terms.
 */
@Component ( {
    selector: 'search-history',
    templateUrl: 'app/components/data/search/search-history/search-history.component.html',
    directives: [SearchAddComponent, Collapse],
    providers: [ SearchService ]
} )

export class SearchHistoryComponent implements OnChanges
{
    /**
     * Search that has been selected
     * @type {EventEmitter}
     */
    @Output () selectedSearch:EventEmitter<any> = new EventEmitter ();

    /**
     * Search term that has been selected.
     * @type {EventEmitter}
     */
    @Output ('searchTermSelected') searchTermSelected:EventEmitter<any> = new EventEmitter ();


    @Input () searchHistoryType:string;

    /**
     * State depends on the data collection and the data evaluation
     */
    @Input () state:string;

    /**
     * List of Topics
     * @type {Array}
     */
    searchHistory:SearchTopic[] = [];
    public errorMessage:string;

    public isCollapsedAvailableData:boolean = false;

    /**
     * view search-add
     */
    isSearchAdd:boolean = false;

    topic:SearchTopic;

    constructor( private searchService:SearchService )
    {
        this.topic = new SearchTopic ();
    }

    ngOnChanges():any
    {
        this.getSearchHistory ();
    }

    getSearchHistory()
    {
        this.searchService.getSearchHistory ().subscribe (
            topics => this.searchHistory = topics,
            error => this.errorMessage = <any>error
        );
    }

    selectSearch( item:SearchDone, term?:SearchTerm )
    {
        this.isCollapsedAvailableData = !this.isCollapsedAvailableData;
        this.getSearchHistory();
        this.selectedSearch.emit ( item );
        if(term)
        {
            this.searchTermSelected.emit(term);
        }
        else
        {
            this.searchTermSelected.emit(this.searchService.getTermById(item.searchTermId));
        }
    }

    addSearch( topic?:SearchTopic )
    {
        if ( topic )
        {
            this.topic = topic
        }
        else
        {
            this.topic = new SearchTopic();
        }
        this.isSearchAdd = true;
    }

    navigateBackToHistory()
    {
        this.isSearchAdd = false;
    }

    selectNewTerm(term)
    {
        this.isSearchAdd = false;
        this.addSearchDone(term);
    }

    addSearchDone(term:SearchTerm)
    {
        let searchDone = new SearchDone();
        searchDone.searchTermId = term.id;
        this.searchService.addSearchDone(searchDone).subscribe (
            searchDone => this.selectSearch(searchDone, term),
            error => this.errorMessage = <any>error
        );
    }

    getCollapseButtonText()
    {
        if(!this.isCollapsedAvailableData)
        {
            return 'hide available data';
        }
        return 'show available data';
    }

    //TODO implement search in tree
}
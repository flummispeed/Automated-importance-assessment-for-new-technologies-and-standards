/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {Component} from '@angular/core';

import {SearchHistoryComponent} from "../search/search-history/search-history.component";
import {SearchDone} from "../../../class/search/search-done";
import {SearchTerm} from "../../../class/search/search-term";
import {DataViewComponent} from "../data-view/data-view.component";


@Component ( {
    selector: 'data-edit',
    templateUrl: 'app/components/data/data-edit/data-edit.component.html',
    directives: [ SearchHistoryComponent, DataViewComponent ]
} )

/**
 * Class DataEditComponent
 * Collects data to a chosen term of a topic at a defined point of time.
 */
export class DataEditComponent
{
    /**
     * State of the component. For example 'Search' for querying the search term or 'Collect Data' for data collection.
     * @type {string}
     */
    state:string = 'Search history';

    searchHistoryType:string = 'Collect Data';

    selectedSearch:SearchDone = new SearchDone ();

    searchTerm:SearchTerm = new SearchTerm ();


    selectSearch( selectedSearch )
    {
        this.state = 'Collect Data';
        this.selectedSearch = selectedSearch;
    }

    selectSearchTerm( term )
    {
        this.searchTerm = term;
    }

    setState( state )
    {
        this.state = state;
    }

}
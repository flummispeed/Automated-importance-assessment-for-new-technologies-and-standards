/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var search_done_1 = require("../../../class/search/search-done");
var search_term_1 = require("../../../class/search/search-term");
var search_history_component_1 = require("../search/search-history/search-history.component");
var data_view_component_1 = require("../data-view/data-view.component");
var DataEvaluationComponent = (function () {
    function DataEvaluationComponent() {
        this.searchHistoryType = 'Evaluate Data';
        /**
         * State of the component. For example 'Search' for querying the search term or 'Collect Data' for data collection.
         * @type {string}
         */
        this.state = 'search history';
        this.selectedSearch = new search_done_1.SearchDone();
        this.searchTerm = new search_term_1.SearchTerm();
    }
    DataEvaluationComponent.prototype.selectSearch = function (selectedSearch) {
        this.selectedSearch = selectedSearch;
        this.state = 'Evaluate Data';
    };
    DataEvaluationComponent.prototype.selectSearchTerm = function (term) {
        this.searchTerm = term;
    };
    DataEvaluationComponent = __decorate([
        core_1.Component({
            selector: 'evaluation',
            templateUrl: 'app/components/data/data-evaluation/data-evaluation.component.html',
            directives: [search_history_component_1.SearchHistoryComponent, data_view_component_1.DataViewComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], DataEvaluationComponent);
    return DataEvaluationComponent;
}());
exports.DataEvaluationComponent = DataEvaluationComponent;
//# sourceMappingURL=data-evaluation.component.js.map
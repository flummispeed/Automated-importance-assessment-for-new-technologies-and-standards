/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import { Injectable }     from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import { Model } from '../../class/model/model';


/**
 * Service to get the importance model from the server.
 */
@Injectable()
export class ModelService
{
    constructor(private http: Http){}

    /**
     * Request the importance model
     * @returns {Observable<R>}
     */
    getModel():Observable<Model>
    {
        let url = '../../server/clientFunc/getModel.php';
        return this.http.get ( url )
            .map ( this.extractData )
            .catch ( this.handleError );
    }

    /**
     * Extract the JSON model in a type script one.
     * @param res
     * @returns {Model}
     */
    private extractData( res:Response )
    {
        let body = res.json ();
        let model = new Model();
        model.deserialize(body);
        return model;
    }

    /**
     * Handle errors
     * @param error
     * @returns {ErrorObservable}
     */
    private handleError( error:any )
    {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error ( errMsg ); // log to console instead
        return Observable.throw ( errMsg );
    }
}
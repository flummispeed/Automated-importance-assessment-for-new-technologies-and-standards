/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var search_value_1 = require('../../class/search/search-value');
var search_topic_1 = require('../../class/search/search-topic');
var Observable_1 = require('rxjs/Observable');
var search_term_1 = require('../../class/search/search-term');
var search_done_1 = require("../../class/search/search-done");
var SearchService = (function () {
    function SearchService(http) {
        this.http = http;
    }
    SearchService.prototype.getTermById = function (termId) {
        for (var _i = 0, _a = SearchService.searchHistoryBuffer; _i < _a.length; _i++) {
            var topic = _a[_i];
            for (var _b = 0, _c = topic.terms; _b < _c.length; _b++) {
                var term = _c[_b];
                if (term.id == termId) {
                    return term;
                }
            }
        }
        return new search_term_1.SearchTerm();
    };
    SearchService.getTopicById = function (topicId) {
        for (var _i = 0, _a = SearchService.searchHistoryBuffer; _i < _a.length; _i++) {
            var topic = _a[_i];
            if (topic.id == topicId) {
                return topic;
            }
        }
        return new search_topic_1.SearchTopic();
    };
    SearchService.prototype.getAutoFill = function (table, foreignKey) {
        var url = '../../server/clientFunc/getAutofill.php';
        if (table) {
            url = url + "?table=" + table;
        }
        if (foreignKey) {
            url = url + "&foreignkey=" + foreignKey;
        }
        return this.http.get(url)
            .map(this.extractAutoFillData)
            .catch(this.handleError);
    };
    SearchService.prototype.getSearchHistory = function (model) {
        var url = '../../server/clientFunc/getSearchHistory.php';
        if (model) {
            url = url + "?model=" + model;
        }
        return this.http.get(url)
            .map(this.extractSearchHistoryData)
            .catch(this.handleError);
    };
    SearchService.prototype.addSearchTopic = function (searchTopic) {
        var url = '../../server/clientFunc/searchTopic.php';
        var body = JSON.stringify(searchTopic);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(url, body, options)
            .map(this.extractSearchTopic)
            .catch(this.handleError);
    };
    SearchService.prototype.addSearchTerm = function (searchTerm) {
        var url = '../../server/clientFunc/searchTerm.php';
        var body = JSON.stringify(searchTerm);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(url, body, options)
            .map(this.extractSearchTerm)
            .catch(this.handleError);
    };
    SearchService.prototype.addSearchDone = function (searchDone) {
        var url = '../../server/clientFunc/searchDone.php';
        var body = JSON.stringify(searchDone);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(url, body, options)
            .map(this.extractSearchDone)
            .catch(this.handleError);
    };
    SearchService.prototype.extractSearchDone = function (res) {
        var body = res.json();
        var searchDone = new search_done_1.SearchDone();
        searchDone.id = body.id;
        searchDone.searchTermId = body.searchTermId;
        searchDone.timestamp = body.timestamp;
        return searchDone;
    };
    SearchService.prototype.extractSearchTerm = function (res) {
        var body = res.json();
        var searchTerm = new search_term_1.SearchTerm();
        searchTerm.id = body.id;
        searchTerm.term = body.term;
        searchTerm.searchTopicId = body.searchTopicId;
        return searchTerm;
    };
    SearchService.prototype.extractSearchTopic = function (res) {
        var body = res.json();
        var searchTopic = new search_topic_1.SearchTopic();
        searchTopic.id = body.id;
        searchTopic.topic = body.topic;
        searchTopic.model = body.model;
        return searchTopic;
    };
    SearchService.prototype.extractAutoFillData = function (res) {
        var body = res.json();
        var autoFills = [];
        for (var _i = 0, body_1 = body; _i < body_1.length; _i++) {
            var item = body_1[_i];
            var searchValue = new search_value_1.SearchValue();
            searchValue.id = item.id;
            searchValue.value = item.value;
            autoFills.push(searchValue);
        }
        return autoFills;
    };
    SearchService.prototype.extractSearchHistoryData = function (res) {
        var body = res.json();
        var topics = [];
        for (var _i = 0, body_2 = body; _i < body_2.length; _i++) {
            var item = body_2[_i];
            var topic = new search_topic_1.SearchTopic();
            topic.id = item.id * 1;
            topic.topic = item.topic;
            topic.model = item.model * 1;
            for (var _a = 0, _b = item.terms; _a < _b.length; _a++) {
                var itemTerm = _b[_a];
                var term = new search_term_1.SearchTerm();
                term.id = itemTerm.id * 1;
                term.term = itemTerm.term;
                term.searchTopicId = itemTerm.searchTopicId * 1;
                for (var _c = 0, _d = itemTerm.searchDone; _c < _d.length; _c++) {
                    var itemSearch = _d[_c];
                    var search = new search_done_1.SearchDone();
                    search.id = itemSearch.id * 1;
                    search.searchTermId = itemSearch.searchTermId * 1;
                    search.timestamp = itemSearch.timestamp;
                    term.searchDone.push(search);
                }
                topic.terms.push(term);
            }
            topics.push(topic);
        }
        SearchService.searchHistoryBuffer = topics;
        return topics;
    };
    SearchService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable_1.Observable.throw(errMsg);
    };
    SearchService.searchHistoryBuffer = [];
    SearchService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], SearchService);
    return SearchService;
}());
exports.SearchService = SearchService;
//# sourceMappingURL=search.service.js.map
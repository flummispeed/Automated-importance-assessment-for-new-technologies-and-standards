/**
* Copyright 2016, Georg-August-Universität Göttingen
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not use
* this file except in compliance with the License. You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software distributed under the
* License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions
* and limitations under the License.
*
* Information
* Master thesis title: Automated importance assessment for new technologies and standards
*
* University: Georg August Universität Göttingen - Institute of Computer Science
* Software Engineering for Distributed Systems
*
* Author: Florian Unger
* Submission date: 2016-11-11
*
*/

import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

import {SearchValue} from '../../class/search/search-value';
import {SearchTopic} from '../../class/search/search-topic';
import {Observable}     from 'rxjs/Observable';
import {SearchTerm} from '../../class/search/search-term';
import {SearchDone} from "../../class/search/search-done";

/**
 * Class to handle the requests to the server
 */
@Injectable ()
export class SearchService
{
    /**
     * Array of previous used toipics
     * @type {Array}
     */
    public static searchHistoryBuffer:SearchTopic[] = [];

    constructor( private http:Http )
    {
    }

    /**
     * Get the name of a search term by its id.
     * @param termId
     * @returns {SearchTerm}
     */
    getTermById(termId)
    {
        for(var topic of SearchService.searchHistoryBuffer)
        {
            for(var term of topic.terms)
            {
                if(term.id == termId)
                {
                    return term;
                }
            }
        }
        return new SearchTerm();
    }

    /**
     * Get the name of a topic by its id.
     * @param topicId
     * @returns {SearchTopic}
     */
    public static getTopicById(topicId):SearchTopic
    {
        for(var topic of SearchService.searchHistoryBuffer)
        {
            if(topic.id == topicId)
            {
                return topic;
            }
        }
        return new SearchTopic();
    }

    /**
     * Provide terms and topics to autocomplete new search.
     * @param table
     * @param foreignKey
     * @returns {Observable<R>}
     */
    getAutoFill( table?:string, foreignKey?:number ):Observable<SearchValue[]>
    {
        let url = '../../server/clientFunc/getAutofill.php';
        if ( table )
        {
            url = url + "?table=" + table;
        }
        if ( foreignKey )
        {
            url = url + "&foreignkey=" + foreignKey;
        }
        return this.http.get ( url )
            .map ( this.extractAutoFillData )
            .catch ( this.handleError );
    }

    /**
     * Request the search history
     * @param model
     * @returns {Observable<R>}
     */
    getSearchHistory( model?:string ):Observable<SearchTopic[]>
    {
        let url = '../../server/clientFunc/getSearchHistory.php';
        if ( model )
        {
            url = url + "?model=" + model;
        }
        return this.http.get ( url )
            .map ( this.extractSearchHistoryData )
            .catch ( this.handleError );
    }

    /**
     * Sends a new topic to the server
     * @param searchTopic
     * @returns {Observable<R>}
     */
    addSearchTopic( searchTopic:SearchTopic ):Observable<SearchTopic>
    {
        let url = '../../server/clientFunc/searchTopic.php';
        let body = JSON.stringify ( searchTopic );
        let headers = new Headers ( { 'Content-Type': 'application/json' } );
        let options = new RequestOptions ( { headers: headers } );

        return this.http.post ( url, body, options )
            .map ( this.extractSearchTopic )
            .catch ( this.handleError );
    }

    /**
     * Sends a new term to the server
     * @param searchTerm
     * @returns {Observable<R>}
     */
    addSearchTerm( searchTerm:SearchTerm ):Observable<SearchTerm>
    {
        let url = '../../server/clientFunc/searchTerm.php';
        let body = JSON.stringify ( searchTerm );
        let headers = new Headers ( { 'Content-Type': 'application/json' } );
        let options = new RequestOptions ( { headers: headers } );

        return this.http.post ( url, body, options )
            .map ( this.extractSearchTerm )
            .catch ( this.handleError );
    }

    /**
     * add a new search of a term to the server.
     * @param searchDone
     * @returns {Observable<R>}
     */
    addSearchDone( searchDone:SearchDone ):Observable<SearchDone>
    {
        let url = '../../server/clientFunc/searchDone.php';
        let body = JSON.stringify ( searchDone );
        let headers = new Headers ( { 'Content-Type': 'application/json' } );
        let options = new RequestOptions ( { headers: headers } );

        return this.http.post ( url, body, options )
            .map ( this.extractSearchDone )
            .catch ( this.handleError );
    }

    /**
     * Parse search of a term from the JSON object transferred from the server.
     * @param res
     * @returns {SearchDone}
     */
    private extractSearchDone( res:Response )
    {
        let body = res.json ();
        let searchDone = new SearchDone ();
        searchDone.id = body.id;
        searchDone.searchTermId = body.searchTermId;
        searchDone.timestamp = body.timestamp;
        return searchDone;
    }

    /**
     * Parse a term from the JSON object transferred from the server.
     * @param res
     * @returns {SearchTerm}
     */
    private extractSearchTerm( res:Response )
    {
        let body = res.json ();
        let searchTerm = new SearchTerm ();
        searchTerm.id = body.id;
        searchTerm.term = body.term;
        searchTerm.searchTopicId = body.searchTopicId;
        return searchTerm;
    }

    /**
     * Parse a toopic from the JSON object transferred from the server.
     * @param res
     * @returns {SearchTopic}
     */
    private extractSearchTopic( res:Response )
    {
        let body = res.json ();
        let searchTopic = new SearchTopic ();
        searchTopic.id = body.id;
        searchTopic.topic = body.topic;
        searchTopic.model = body.model;
        return searchTopic;
    }

    /**
     * Parse sautofill values from the JSON object transferred from the server.
     * @param res
     * @returns {SearchValue[]}
     */
    private extractAutoFillData( res:Response )
    {
        let body = res.json ();
        let autoFills:SearchValue[] = [];
        for ( var item of body )
        {
            let searchValue = new SearchValue ();
            searchValue.id = item.id;
            searchValue.value = item.value;
            autoFills.push ( searchValue );
        }
        return autoFills;
    }

    /**
     * Parse search history from the JSON object transferred from the server.
     * @param res
     * @returns {SearchTopic[]}
     */
    private extractSearchHistoryData( res:Response )
    {
        let body = res.json ();
        let topics:SearchTopic[] = [];
        for ( var item of body )
        {
            let topic = new SearchTopic ();
            topic.id = item.id * 1;
            topic.topic = item.topic;
            topic.model = item.model * 1;

            for ( var itemTerm of item.terms )
            {
                let term = new SearchTerm ();
                term.id = itemTerm.id * 1;
                term.term = itemTerm.term;
                term.searchTopicId = itemTerm.searchTopicId * 1;
                for ( var itemSearch of itemTerm.searchDone )
                {
                    let search = new SearchDone ();
                    search.id = itemSearch.id * 1;
                    search.searchTermId = itemSearch.searchTermId * 1;
                    search.timestamp = itemSearch.timestamp;
                    term.searchDone.push ( search );
                }
                topic.terms.push ( term );
            }
            topics.push ( topic );
        }
        SearchService.searchHistoryBuffer = topics;
        return topics;
    }

    /**
     * Handle errors.
     * @param error
     * @returns {ErrorObservable}
     */
    private handleError( error:any )
    {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error ( errMsg ); // log to console instead
        return Observable.throw ( errMsg );
    }
}
/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */

import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Result} from "../../class/result/result";
import {Observable} from "rxjs/Observable";
import {ResultItem} from "../../class/result/result-item";
import {SearchTerm} from "../../class/search/search-term";

/**
 * Class to store and request the measured data.
 */
@Injectable ()
export class EvaluationDataService
{
    /**
     * General constrctor is necessary.
     * @param http
     */
    constructor( private http:Http )
    {
    }

    /**
     * Request a data set from the server
     * @param searchid
     * @returns {Observable<R>}
     */
    getResult(searchid):Observable<Result>
    {
        let url = '../../server/clientFunc/evaluationData.php?searchid=' + searchid;
        return this.http.get ( url )
            .map ( this.extractData )
            .catch ( this.handleError );
    }

    /**
     * Send a data set to the server.
     * @param result
     * @returns {Observable<R>}
     */
    addData(result:Result):Observable<Result>
    {
        let url = '../../server/clientFunc/evaluationData.php';
        let body = JSON.stringify(result);
        let headers = new Headers ( { 'Content-Type': 'application/json' } );
        let options = new RequestOptions ( { headers: headers } );

        return this.http.post ( url, body, options )
            .map ( this.extractData)
            .catch ( this.handleError );
    }

    /**
     * Exract an data set from JSON
     * @param res
     * @returns {Result}
     */
    extractData(res:Response)
    {
        let result = new Result();
        let body = res.json ();
        result.idSearch = body.idSearch;
        for(var resultItem of body.resultItems)
        {
            let item = new ResultItem();
            item.idEvaluation = resultItem.idEvaluation;
            item.type = resultItem.type;
            item.value = resultItem.value;
            item.valueCanNotBeFound = resultItem.valueCanNotBeFound;
            result.resultItems.push(item);
        }
        let term = new SearchTerm();
        term.id = body.term.id * 1;
        term.searchTopicId = body.term.searchTopicId *1;
        term.term = body.term.term;
        result.term = term;
        result.searchTime = body.searchTime;
        return result;
    }

    /**
     * Handle errors
     * @param error
     * @returns {ErrorObservable}
     */
    private handleError( error:any )
    {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error ( errMsg ); // log to console instead
        return Observable.throw ( errMsg );
    }

}
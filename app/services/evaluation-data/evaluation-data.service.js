/**
 * Copyright 2016, Georg-August-Universität Göttingen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * Information
 * Master thesis title: Automated importance assessment for new technologies and standards
 *
 * University: Georg August Universität Göttingen - Institute of Computer Science
 * Software Engineering for Distributed Systems
 *
 * Author: Florian Unger
 * Submission date: 2016-11-11
 *
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var result_1 = require("../../class/result/result");
var Observable_1 = require("rxjs/Observable");
var result_item_1 = require("../../class/result/result-item");
var search_term_1 = require("../../class/search/search-term");
var EvaluationDataService = (function () {
    function EvaluationDataService(http) {
        this.http = http;
    }
    EvaluationDataService.prototype.getResult = function (searchid) {
        var url = '../../server/clientFunc/evaluationData.php?searchid=' + searchid;
        return this.http.get(url)
            .map(this.extractData)
            .catch(this.handleError);
    };
    EvaluationDataService.prototype.addData = function (result) {
        var url = '../../server/clientFunc/evaluationData.php';
        var body = JSON.stringify(result);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(url, body, options)
            .map(this.extractData)
            .catch(this.handleError);
    };
    EvaluationDataService.prototype.extractData = function (res) {
        var result = new result_1.Result();
        var body = res.json();
        result.idSearch = body.idSearch;
        for (var _i = 0, _a = body.resultItems; _i < _a.length; _i++) {
            var resultItem = _a[_i];
            var item = new result_item_1.ResultItem();
            item.idEvaluation = resultItem.idEvaluation;
            item.type = resultItem.type;
            item.value = resultItem.value;
            item.valueCanNotBeFound = resultItem.valueCanNotBeFound;
            result.resultItems.push(item);
        }
        var term = new search_term_1.SearchTerm();
        term.id = body.term.id * 1;
        term.searchTopicId = body.term.searchTopicId * 1;
        term.term = body.term.term;
        result.term = term;
        result.searchTime = body.searchTime;
        return result;
    };
    EvaluationDataService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable_1.Observable.throw(errMsg);
    };
    EvaluationDataService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], EvaluationDataService);
    return EvaluationDataService;
}());
exports.EvaluationDataService = EvaluationDataService;
//# sourceMappingURL=evaluation-data.service.js.map